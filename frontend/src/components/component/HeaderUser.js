import { React, useEffect, useState } from 'react'
import axios from "axios";
import Loading from "react-loading"
import { useNavigate } from "react-router-dom";

function HeaderUser({ userClickProfile }) {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [mydata, setMydata] = useState([]);
    const token = localStorage.getItem('TokenUser')
    // console.log("Mydata:", mydata)
    // console.log("token:", token)

    //---------------------------- fetch API Mydata --------------------------------//
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/headerUser/${token}`)
                .then((resp) => {
                    setMydata(resp.data[0]);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();

    }, [mydata.id, token]);

    //---------------- Logout ---------------//
    const signOut = () => {
        // localStorage.clear();
        localStorage.removeItem("TokenUser")
        navigate("/");
    };

    //---------------- Loading ---------------//
    if (loading) {
        return (
            <div className="grid">
                <div className="grid justify-items-center mt-80">
                    <Loading type={"bar"} color={"#92B4EC"} height={400} width={200} />
                </div>
            </div>
        );
    }


    return (

        <nav className="
  relative
  w-full
  flex flex-wrap
  items-center
  justify-between
  py-4
  bg-gray-100
  shadow-lg
  navbar navbar-expand-lg navbar-light
  ">
            <div className="container-fluid w-full flex items-center justify-between px-6">
                <div className='font-bold text-[20px]'>ระบบจองคิวออเดอร์วาดรูป</div>
                <div className="flex items-center relative">
                    <a className="text-gray-500 hover:text-gray-700 focus:text-gray-700 mr-4" href="/Home">
                        หน้าแรก
                    </a>
                    <a className="text-gray-500 hover:text-gray-700 focus:text-gray-700 mr-8" href="/Home/Mytable">
                        ตารางการจอง
                    </a>
                    <div className="dropdown relative">
                        <a className="dropdown-toggle flex items-center hidden-arrow  focus:ring focus:ring-cyan-500 focus:rounded-full" id="dropdownMenuButton2" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            <img src={mydata.image} className="rounded-full h-[35px] w-[35px]"
                                alt="" loading="lazy" />

                            {/* <p className='ml-1 font-medium text-base'>{mydata.name}</p>
                                <svg className="ml-1 mt-0.5 w-4 h-4" aria-hidden="true" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7"></path></svg> */}
                        </a>
                        <ul className="
        dropdown-menu
        w-[300px]
        absolute
        hidden
        bg-white
        text-base
        z-50
        float-left
        py-3
        px-5
        list-none
        text-left
        rounded-lg
        shadow-lg
        mt-1
        m-0
        bg-clip-padding
        border-none
        left-auto
        right-0
      " aria-labelledby="dropdownMenuButton2">
                            <div className='flex w-full mb-1 items-center'>
                                <div className='grid items-center'>
                                    <img src={mydata.image} className="rounded-full h-[40px] w-[40px]"
                                        alt="" loading="lazy" />
                                </div>
                                <div className=''>
                                    <h1 className='ml-4 font-medium text-lg'>{mydata.name}</h1>
                                    <h1 className='ml-4 text-base'>{mydata.email}</h1>
                                </div>
                            </div>

                            <div className='grid justify-items-center'>
                                <hr className='w-full' />
                            </div>

                            <li>
                                <button className="
            dropdown-item
            text-sm
            text-left
            mt-3
            py-2
            font-normal
            block
            w-full
            whitespace-nowrap
            bg-transparent
            text-gray-700
            hover:bg-gray-100
          "  onClick={() => userClickProfile(mydata.id)}>โปรไฟล์</button>
                            </li>


                            <li>
                                <button className="
            dropdown-item
            text-sm
            text-left
            py-2
            font-normal
            block
            w-full
            whitespace-nowrap
            bg-transparent
            text-gray-700
            hover:bg-gray-100" onClick={signOut}>ออกจากระบบ</button>
                            </li>
                        </ul>
                    </div>
                </div>
                {/* <!-- Right elements --> */}
            </div>

        </nav>


    )
}

export default HeaderUser