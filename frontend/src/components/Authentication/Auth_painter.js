import {useEffect} from 'react'
import axios from "axios";
export function RequireTokenPainter({ children }) {
    const token = localStorage.getItem('TokenPainter')

    useEffect(() => {
        axios
            .post("http://localhost:8000/painter/Authentication", {
                token: token,
            })
            .then((resp) => {
                // console.log("resp:", resp.data)
                if (resp.data.status === "ok") {
                    console.log("------------------ Pass!! --------------------")
                } else {
                    localStorage.removeItem('TokenPainter')
                    // localStorage.clear()
                    alert("Token Error!!")
                    window.location = "/LoginPainter"
                }
            })
            .catch((err) => console.log(err));

        if (!token) {
            window.location = "/LoginPainter"
        }

    }, [token]);


    return children;
}