import {useEffect} from 'react'
import axios from "axios";
export function RequireTokenAdmin({ children }) {
    const token = localStorage.getItem('TokenAdmin')

    useEffect(() => {
        axios
            .post("http://localhost:8000/admin/Authentication", {
                token: token,
            })
            .then((resp) => {
                // console.log("resp:", resp.data)
                if (resp.data.status === "ok") {
                    console.log("------------------ Pass!! --------------------")
                } else {
                    localStorage.removeItem('TokenAdmin')
                    // localStorage.clear()
                    alert("Token Error!!")
                    window.location = "/LoginAdmin"
                }
            })
            .catch((err) => console.log(err));

        if (!token) {
            window.location = "/LoginAdmin"
        }

    }, [token]);


    return children;
}