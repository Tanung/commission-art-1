import axios from "axios";
import Loading from "react-loading";
import { React, useEffect, useState } from "react";

function PopupEdituser({ popupEdit, setPopupEdit, id_user }) {
    const [dataUser, setDataUser] = useState('');
    const [loading, setLoading] = useState(true);

    //---------------- get api user ---------------//
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/admin/get/user/${id_user}`)
                .then((resp) => {
                    setDataUser(resp.data[0]);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [id_user]);

    //---------------- OnChange ---------------//
    const onChangeEdit = (e) => {
        e.preventDefault();
        const newData = { ...dataUser };
        newData[e.target.name] = e.target.value;
        setDataUser(newData)
    }


    //---------------- onSubmit ---------------//
    const clikEditUser = (e) => {
        e.preventDefault();
        axios.put(`http://localhost:8000/admin/update/user/${id_user}`, {
            name: dataUser.name,
            username: dataUser.username,
            email: dataUser.email,
            phone: dataUser.phone
        })

            .then((res) => {
                // console.log(res.data);
                alert("สำเร็จ")
                setPopupEdit(!popupEdit)
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });
    }

    return (
        <div className="justify-center py-8 items-center text-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className="bg-white rounded-lg h-auto w-[50%] text-black">
                    <div className="px-6 py-6 h-auto">
                        <form onSubmit={clikEditUser}>
                            <h1 className="mb-8 text-3xl text-center">Profile</h1>
                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="name"
                                value={dataUser.name}
                                onChange={onChangeEdit}
                            />

                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="username"
                                value={dataUser.username}
                                onChange={onChangeEdit}
                            />

                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="email"
                                value={dataUser.email}
                                onChange={onChangeEdit}
                            />

                            <input
                                type="number"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="phone"
                                value={dataUser.phone}
                                onChange={onChangeEdit}
                            />

                            <div className='flex items-end justify-center space-x-5'>
                                <div className="grid font-skvsb">
                                    <button
                                        type="submit"
                                        className="bg-[#5FCB39] text-white rounded w-24 h-8"
                                    >
                                        Edit
                                    </button>
                                </div>
                                <div className="grid font-skvsb">
                                    <button
                                        className="bg-gray-400 text-white rounded w-24 h-8"
                                        onClick={() => setPopupEdit(!popupEdit)}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            )}
        </div>
    )
}

export default PopupEdituser