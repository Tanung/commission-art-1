import axios from "axios";
import Loading from "react-loading";
import { React, useEffect, useState } from "react";

function EditPainter({ popupEdit, setPopupEdit, id_painter }) {
    const [dataPainter, setDataPainter] = useState('');
    const [loading, setLoading] = useState(true);

    //---------------- get api painter ---------------//
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/admin/get/painter/${id_painter}`)
                .then((resp) => {
                    setDataPainter(resp.data.data[0]);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [id_painter]);

    //---------------- OnChange ---------------//
    const onChangeEdit = (e) => {
        e.preventDefault();
        const newData = { ...dataPainter };
        newData[e.target.name] = e.target.value;
        setDataPainter(newData)
    }


    //---------------- onSubmit ---------------//
    const clikEditPainter = (e) => {
        e.preventDefault();
        axios.put(`http://localhost:8000/admin/update/painter/${id_painter}`, {
            name: dataPainter.painter_name,
            username: dataPainter.painter_username,
            email: dataPainter.painter_email,
            phone: dataPainter.painter_phone
        })
            .then((res) => {
                // console.log(res.data);
                alert("สำเร็จ")
                setPopupEdit(!popupEdit)
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });
    }

    return (

        <div className="justify-center py-8 items-center text-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className="bg-white rounded-lg h-auto w-[50%] text-black">
                    <div className="px-6 py-6 h-auto">
                        <form onSubmit={clikEditPainter}>
                            <h1 className="mb-8 text-3xl text-center">Profile</h1>
                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="painter_name"
                                value={dataPainter.painter_name}
                                onChange={onChangeEdit}
                            />

                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="painter_username"
                                value={dataPainter.painter_username}
                                onChange={onChangeEdit}
                            />

                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="painter_email"
                                value={dataPainter.painter_email}
                                onChange={onChangeEdit}
                            />

                            <input
                                type="number"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="painter_phone"
                                value={dataPainter.painter_phone}
                                onChange={onChangeEdit}
                            />

                            <div className='flex items-end justify-center space-x-5'>
                                <div className="grid font-skvsb">
                                    <button
                                        type="submit"
                                        className="bg-[#5FCB39] text-white rounded w-24 h-8"
                                    >
                                        Edit
                                    </button>
                                </div>
                                <div className="grid font-skvsb">
                                    <button
                                        className="bg-gray-400 text-white rounded w-24 h-8"
                                        onClick={() => setPopupEdit(!popupEdit)}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            )}
        </div>
    )
}

export default EditPainter