import { React, useEffect, useState } from 'react'
import axios from "axios";
import Loading from "react-loading"
// import { useNavigate } from "react-router-dom";
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import HeaderAdmin from "../../component/HeaderAdmin"
import NavbarAdmin from './component/NavbarAdmin';
import PopupDeletepainter from './component/PopupDeletepainter';
import PopupEditpainter from './component/PopupEditpainter';

function PainterPage() {
    // const navigate = useNavigate();
    const [dataPainter, setDataPainter] = useState([]);
    const [id_painter, setId_painter] = useState('')
    const [popupDelete, setPopupDelete] = useState(true)
    const [popupEdit, setPopupEdit] = useState(true)
    const [loading, setLoading] = useState(true);
    // console.log("dataPainter:", dataPainter)

    //---------------------------- fetch API painters --------------------------------//
    useEffect(() => {
        GetAPIusers();
    }, []);

    const GetAPIusers = async () => {
        await axios
            .get("http://localhost:8000/api/painters")
            .then((resp) => {
                // console.log(resp.data.data);
                setDataPainter(resp.data.data);
                setLoading(false);
            })
            .catch((err) => console.log(err));
    };

    //------------------ Popup delete painter ---------------------///
    const popupDeletePainter = (id) => {
        setId_painter(id)
        setPopupDelete(!popupDelete)
    }

    ///------------------ Popup edit package ---------------------///
    const popupEditPainter = (id) => {
        setId_painter(id)
        setPopupEdit(!popupEdit)
    }

    return (
        <div className='bg-gray-200 w-full h-screen'>
            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className='bg-gray-200'>

                    {!popupDelete ?
                        <PopupDeletepainter popupDelete={popupDelete} setPopupDelete={setPopupDelete} id_painter={id_painter} />
                        : null
                    }

                    {!popupEdit ?
                        <PopupEditpainter popupEdit={popupEdit} setPopupEdit={setPopupEdit} id_painter={id_painter} />
                        : null
                    }

                    <HeaderAdmin />
                    {/* <!-- component --> */}
                    <div className='px-5 my-5 mx-auto container h-screen'>
                        <NavbarAdmin />

                        <div className='mt-5'>
                            <table className="min-w-full text-center border-collapse block md:table">
                                <thead className="block md:table-header-group">
                                    <tr className="border border-grey-500 md:border-none block md:table-row absolute -top-full md:top-auto -left-full md:left-auto  md:relative ">
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ลำดับ</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ชื่อ</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ชื่อผู้ใช้</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">อีเมล์</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">เบอร์โทร</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">Actions</th>
                                    </tr>
                                </thead>
                                {dataPainter.length === 0 ? (
                                    <tbody className="flex h-full justify-center mt-32 space-x-2 font-skvb">
                                        <tr className=''>
                                            <td>
                                                <FontAwesomeIcon
                                                    icon={faTriangleExclamation}
                                                    className="text-yellow-300 mt-1 w-7 h-7"
                                                />
                                                <h1 className="mt-2">No Data Found</h1>
                                            </td>
                                        </tr>
                                    </tbody>
                                ) : (
                                    dataPainter.map((data, key) => {
                                        return (

                                            <tbody key={key} className="bg-white block md:table-row-group">
                                                <tr className="border border-grey-500 md:border-none block md:table-row">
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold"></span>{key + 1}</td>
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">Name</span>{data.painter_name}</td>
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">User Name</span>{data.painter_username}</td>
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">Email Address</span>{data.painter_email}</td>
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">Mobile</span>0855555555</td>
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell">
                                                        <span className="inline-block w-1/3 md:hidden font-bold">Actions</span>
                                                        <button onClick={() => popupEditPainter(data.painter_id)} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 border border-blue-500 rounded">Edit</button>
                                                        <button onClick={() => popupDeletePainter(data.painter_id)} className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-500 rounded">Delete</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        )
                                    })
                                )}
                            </table>

                        </div>
                    </div>
                </div>
            )}

        </div>
    )
}

export default PainterPage