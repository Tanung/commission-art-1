import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

import Header from "../../component/Header"

function RegisterUser() {
    const navigate = useNavigate();

    //-----------------------------------  useState --------------------------------------///
    const [createUser, setCreateUser] = useState({
        name: "",
        username: "",
        email: "",
        phone: "",
        password: "",
        confirm_password: "",
    })
    const [image, setImage] = useState('');
    // console.log("Data:", createUser)
    // console.log("Image:", image)

    //----------------------------------- รับข้อมูลจาก input --------------------------------------///
    const onChangeUser = (e) => {
        e.preventDefault();
        const newUser = { ...createUser };
        newUser[e.target.name] = e.target.value;
        setCreateUser(newUser)
    }

    //----------------------------------- รับค่า Number Phone จาก input --------------------------------------///
    const onChangePhoneNumber = (e) => {
        e.preventDefault();
        const numberPhone = { ...createUser };
        numberPhone[e.target.name] = e.target.value.slice(0, 10);
        setCreateUser(numberPhone)
    }

    //----------------------------------- แปลงรูปเปลี่ยนเป็น base64 --------------------------------------///
    const Imagebase64 = async (e) => {
        e.preventDefault()
        const file = e.target.files[0];
        const reader = new FileReader();
        // console.log("file:", file)

        await reader.readAsDataURL(file)

        reader.onloadend = () => {
            setImage(reader.result.toString());
        };
    }

    const removeImage = () => {
        setImage('');
    }

    //------------------ ส่งข้อมูลไป backend ---------------------///
    const Submit = async (e) => {
        e.preventDefault();
        if (createUser.name === '') {
            e.preventDefault();
            alert("กรุณากรอกชื่อ");
        } else if (createUser.username === '') {
            e.preventDefault();
            alert("กรุณากรอกชื่อผู้ใช้");
        } else if (createUser.email === '') {
            e.preventDefault();
            alert("กรุณากรอกอีเมล์");
        } else if (createUser.phone === '') {
            e.preventDefault();
            alert("กรุณากรอกเบอร์โทร");
        } else if (createUser.password === '') {
            e.preventDefault();
            alert("กรุณากรอกรหัสผ่าน");
        } else if (createUser.confirm_password === '') {
            e.preventDefault();
            alert("กรุณากรอกยืนยันรหัสผ่าน");
        } else if (createUser.password !== createUser.confirm_password) {
            e.preventDefault();
            alert("รหัสผ่านไม่ตรงกัน");
        } else if (image.length === 0) {
            e.preventDefault();
            alert("กรุณาเลือกรูปภาพโปรไฟล์");
        } else {
            e.preventDefault();
            // console.log("ผ่านๆๆๆๆ")
            axios.post("http://localhost:8000/user/register", {
                name: createUser.name,
                username: createUser.username,
                email: createUser.email,
                phone: createUser.phone,
                password: createUser.password,
                image: image,
            })
                .then((res) => {
                    console.log("Response:", res.data);
                    alert("Create Account Successfull!!");
                    navigate("/");
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }


    return (
        <div className="bg-grey-lighter min-h-screen flex flex-col">
            <Header />
            <div className="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
                <div className="bg-white mt-7 mb-5 px-6 py-8 rounded shadow-md text-black w-full">
                    <form onSubmit={Submit}>
                        <h1 className="mb-8 text-3xl text-center">Sign up</h1>
                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="name"
                            value={createUser.name}
                            onChange={onChangeUser}
                            placeholder="Name" />

                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="username"
                            value={createUser.username}
                            onChange={onChangeUser}
                            placeholder="Username" />

                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="email"
                            value={createUser.email}
                            onChange={onChangeUser}
                            placeholder="Email" />

                        <input
                            type="number"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="phone"
                            value={createUser.phone}
                            onChange={onChangePhoneNumber}
                            placeholder="Phone Number" />

                        <input
                            type="password"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="password"
                            value={createUser.password}
                            onChange={onChangeUser}
                            placeholder="Password" />

                        <input
                            type="password"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="confirm_password"
                            value={createUser.confirm_password}
                            onChange={onChangeUser}
                            placeholder="Confirm Password" />

                        <div className='mb-2'>
                            <input type="file" onChange={(e) => Imagebase64(e)} />
                            {image.length !== 0 ?
                                (<div className="overflow-hidden relative mt-2">
                                    <div onClick={removeImage} className="mdi mdi-close absolute ml-[67px] hover:text-red-500 cursor-pointer">x</div>
                                    <img className="h-20 w-20 rounded-md" src={image} alt="" />
                                </div>) :
                                null
                            }
                        </div>

                        <button
                            type="submit"
                            className="w-full text-center py-3 bg-green-400 rounded text-white hover:bg-green-500 focus:outline-none my-1"
                        >Create Account</button>

                        {/* <div className="text-center text-sm text-grey-dark mt-4">
                        By signing up, you agree to the
                        <a className="no-underline border-b border-grey-dark text-grey-dark" href="#">
                            Terms of Service
                        </a> and
                        <a className="no-underline border-b border-grey-dark text-grey-dark" href="#">
                            Privacy Policy
                        </a>
                    </div> */}
                    </form>
                </div>

                <div className="flex space-x-2 text-grey-dark mb-7">
                    <div>
                        Already have an account?
                    </div>
                    <a className="no-underline text-red-600  hover:text-red-700 focus:text-red-700 border-b border-blue text-blue" href="/">
                        Log in
                    </a>.
                </div>
            </div>
        </div>
    )
}

export default RegisterUser