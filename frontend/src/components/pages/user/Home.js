import { React, useEffect, useState } from 'react'
import axios from "axios";
import Loading from "react-loading"
import { useNavigate } from "react-router-dom";
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// import Header from "../../component/Header"
import HeaderUser from "../../component/HeaderUser"

function Home() {
    const navigate = useNavigate();
    const [dataPainter, setDataPainter] = useState([]);
    const [dataImage, setDataImage] = useState([]);
    const [search, setSearch] = useState("");
    const [loading, setLoading] = useState(true);
    const token = localStorage.getItem('TokenUser')
    const uniqueImages = [];
    
    useEffect(() => {

        //---------------------------- fetch API painters --------------------------------//
        (async () => {
            await axios
                .get("http://localhost:8000/api/painters")
                .then((resp) => {
                    // console.log("resp:", resp.data.image2)
                    setDataPainter(resp.data.data);
                    setDataImage(resp.data.images);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [token]);

    //---------------------------- ดึงรูปที่ไม่ซ้ำกัน --------------------------------//
    const img_painter = dataImage.filter(img => {
        const isDuplicate = uniqueImages.includes(img.painter_id);

        if (!isDuplicate) {
            uniqueImages.push(img.painter_id);

            return true;
        }

        return false;
    });

    //---------------------------- Search ----------------------------//
    const handleSearch = async (e) => {
        e.preventDefault();
        setSearch(e.target.searchName.value);
    };

    //---------------- Nevigate params ---------------//
    const userClickDetail = (data) => {
        navigate(`/Home/Detail/${data}`);
    };

    //---------------- Nevigate params profile user ---------------//
    const userClickProfile = (data) => {
        navigate(`/Home/Profile/${data}`);
    };

    return (

        <div className='bg-gray-200 w-full h-screen'>

            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <>
                    <HeaderUser userClickProfile={userClickProfile} />
                    <div className='bg-gray-200'>
                        <section className="text-gray-600 body-font">
                            <div className="container px-10 py-10 mx-auto">
                                <form onSubmit={handleSearch} autoComplete="off">
                                    <div className='flex justify-end items-center'>
                                        {/* <div className='font-extrabold text-3xl'>นักวาดทั้งหมด</div> */}
                                        <div className="flex justify-center">
                                            <div className="w-[100%]">
                                                <div className="input-group relative flex flex-wrap items-stretch w-full mb-4">
                                                    <input type="search" name="searchName" className="form-control relative flex-auto min-w-0 block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" placeholder="Search" aria-label="Search" aria-describedby="button-addon2" />
                                                    <button className="btn px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700  focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out flex items-center" type="submit" id="button-addon2">
                                                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" className="w-4" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                            <path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                

                                <div className=''>
                                    <div className=''>
                                        <div className='font-extrabold text-3xl'>นักวาดทั้งหมด</div>
                                        <div className='grid sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 mt-2 -m-4'>
                                            {dataPainter.length === 0 ? (
                                                <div className="mt-80 flex space-x-2 font-skvb">
                                                    <FontAwesomeIcon
                                                        icon={faTriangleExclamation}
                                                        className="text-yellow-300 mt-1 w-7 h-7"
                                                    />
                                                    <h1 className="mt-2">No Data Found</h1>
                                                </div>
                                            ) : (
                                                dataPainter
                                                    .filter((filter_search) => filter_search.painter_name.includes(search))
                                                    .map((data, key) => {
                                                        return (
                                                            <div key={key} onClick={() => userClickDetail(data.painter_id)} className="p-4 w-full">
                                                                <div className="bg-white h-full rounded-md overflow-hidden">
                                                                    <div className='flex items-center px-5 space-x-3 h-16'>
                                                                        <div className=''><img className="w-10 h-10 rounded-3xl object-cover object-center" src={data.painter_image} alt="blog" /></div>
                                                                        <div className='text-lg font-medium'>{data.painter_name}</div>
                                                                    </div>
                                                                    
                                                                    {img_painter.map((img, key) => {
                                                                        return (
                                                                            <div key={key}>
                                                                                {img.painter_id === data.painter_id ? (
                                                                                    <div className=''>
                                                                                        <img className="lg:h-48 md:h-36 w-full object-cover object-center" src={img.image_base64} alt="blog" />
                                                                                    </div>
                                                                                ) : (
                                                                                    <></>
                                                                                    // <img className="lg:h-48 md:h-36 w-full object-cover object-center" src="https://dummyimage.com/720x400" alt="blog" />
                                                                                )}
                                                                            </div>
                                                                        )
                                                                    })}

                                                                    <div className="p-6">
                                                                        <h2 className="tracking-widest text-xs title-font font-medium text-gray-400 mb-1">Style</h2>
                                                                        <h1 className="title-font text-lg font-medium text-gray-900 mb-3">{data.painter_style}</h1>
                                                                        {/* <p className="leading-relaxed mb-3">Photo booth fam</p> */}
                                                                        <div className="flex items-center flex-wrap ">
                                                                            <button onClick={() => userClickDetail(data.painter_id)} className="text-indigo-500 inline-flex items-center md:mb-2 lg:mb-0">See More
                                                                                <svg className="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                                                                    <path d="M5 12h14"></path>
                                                                                    <path d="M12 5l7 7-7 7"></path>
                                                                                </svg>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        )
                                                    })
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </>
            )}

        </div>
    )
}

export default Home