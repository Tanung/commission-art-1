import { React, useEffect, useState } from 'react'
import axios from "axios";
import Loading from "react-loading"
import { useNavigate } from "react-router-dom";
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import HeaderUser from "../../component/HeaderUser"
// import NavbarUser from "./component/NavbarUser"
import PopupMyOrder from "./component/PopupMyOrder";
import PopupProduct from './component/PopupProduct';
import PopupCancel from './component/PopupCancel';
import PopupConfirm from './component/PopupConfirm';


function Mytable() {
    const navigate = useNavigate();
    // const [dataUser, setDataUser] = useState([]);
    const [dataOrder, setOrder] = useState([]);
    const [dataAllStatus, setAllStatus] = useState('');
    const [PopupSeeMyorder, setPopupSeeMyorder] = useState(true)
    const [popupProduct, setPopupProduct] = useState(true)
    const [popupConfirm, setPopupConfirm] = useState(true)
    const [popupCancel, setPopupCancel] = useState(true)
    const [id_order, setId_order] = useState([]);
    const [loading, setLoading] = useState(true);
    const token = localStorage.getItem('TokenUser')
    // console.log("dataOrder:", dataOrder)
    // console.log("ID_package:", id_package)
    // console.log("ID_order:", id_order)
    // console.log("status:", dataAllStatus.map((sss) => sss))
    // dataAllStatus.forEach((sss) => {
    //     console.log("test: ", sss)
    // })

    //---------------------------- fetch API users --------------------------------//
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/MydataUser/${token}`)
                .then((resp) => {
                    // console.log("dataUser:", resp.data)
                    // setDataUser(resp.data.data);
                    setOrder(resp.data.orders);
                    setAllStatus(resp.data.status)
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [token]);

    ///------------------ Popup My order ---------------------///
    const popupMyorder = (id) => {
        setId_order(id)
        setPopupSeeMyorder(!PopupSeeMyorder)
    }

    ///------------------ Popup see product ---------------------///
    const popupSeeProduct = (id) => {
        setId_order(id)
        setPopupProduct(!popupProduct)
    }

    ///------------------ Popup Cancel ---------------------///
    const popup_Cancel = (id) => {
        setId_order(id)
        setPopupCancel(!popupCancel)
    }

    ///------------------ Popup Confirm ---------------------///
    const popup_Confirm = (id) => {
        setId_order(id)
        setPopupConfirm(!popupConfirm)

    }

    //---------------- Nevigate params profile user ---------------//
    const userClickProfile = (data) => {
        navigate(`/Home/Profile/${data}`);
    };

    return (
        <div className='bg-gray-200 w-full h-screen'>

            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className='bg-gray-200'>
                    <HeaderUser userClickProfile={userClickProfile} />

                    {!PopupSeeMyorder ?
                        <PopupMyOrder PopupSeeMyorder={PopupSeeMyorder} setPopupSeeMyorder={setPopupSeeMyorder} id_order={id_order} />
                        : null
                    }

                    {!popupCancel ?
                        <PopupCancel popupCancel={popupCancel} setPopupCancel={setPopupCancel} id_order={id_order} />
                        : null
                    }

                    {!popupConfirm ?
                        <PopupConfirm popupConfirm={popupConfirm} setPopupConfirm={setPopupConfirm} id_order={id_order} />
                        : null
                    }

                    {!popupProduct ?
                        <PopupProduct popupProduct={popupProduct} setPopupProduct={setPopupProduct} id_order={id_order} />
                        : null
                    }

                    <div className='px-5 my-5 mx-auto container h-screen'>
                        <h1 className='mt-10 mb-5 font-semibold text-3xl text-center'>
                            ตารางการจอง
                        </h1>

                        <div className='mt-5'>
                            <table className="min-w-full text-center border-collapse block md:table">
                                <thead className="block md:table-header-group">
                                    <tr className="border border-grey-500 md:border-none block md:table-row absolute -top-full md:top-auto -left-full md:left-auto  md:relative ">
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ลำดับ</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ชื่อนักวาด</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">สไตล์</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ราคา</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">วันที่ได้คิว</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">วันส่งงาน</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">สถานะ</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">Actions</th>
                                    </tr>
                                </thead>

                                {dataOrder.length === 0 ? (
                                    <tbody className="flex h-full justify-center mt-32 space-x-2 font-skvb">
                                        <tr className=''>
                                            <td>
                                                <FontAwesomeIcon
                                                    icon={faTriangleExclamation}
                                                    className="text-yellow-300 mt-1 w-7 h-7"
                                                />
                                                <h1 className="mt-2">No Data Found</h1>
                                            </td>
                                        </tr>
                                    </tbody>
                                ) : dataOrder
                                    // .filter((search) => search.order_status
                                    //     .includes(chooseDropdown))
                                    .map((data, key) => {
                                        return (
                                            <tbody key={key} className="bg-white block md:table-row-group">
                                                <tr className="border border-grey-500 md:border-none block md:table-row">
                                                    <td className="w-10 p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block md:hidden font-bold"></span>{key + 1}</td>
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">Name</span>{data.painter_name}</td>
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">Style</span>{data.painter_style}</td>
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">Price</span>{data.package_price}</td>
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">DateQ</span>{data.order_dateQ}</td>
                                                    <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">Deadline</span>{data.order_deadline}</td>
                                                    <td className="w-[15%] p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">Status</span>
                                                        {dataAllStatus.map((dataStatus, key) => {
                                                            return (
                                                                <div key={key}>
                                                                    {dataStatus.status_id === data.order_status ? dataStatus.status : null}
                                                                </div>
                                                            )
                                                        })}
                                                    </td>

                                                    <td className="w-[20%] p-2 md:border md:border-grey-500 block md:table-cell">
                                                        <span className="inline-block w-1/3 md:hidden font-bold">Actions</span>
                                                        <div className='flex justify-center space-x-1'>

                                                            {data.order_status === 2 ?
                                                                <button onClick={() => popup_Confirm(data.order_id)} className="bg-lime-500 hover:bg-lime-600 text-white font-bold py-1 px-2 border border-lime-400 rounded">ยืนยัน</button>
                                                                : <></>}

                                                            {data.order_status === 3 || data.order_status === 4 || data.order_status === 5 || data.order_status === 6 || data.order_status === 7 || data.order_status === 8 ?
                                                                <></>
                                                                : <button onClick={() => popup_Cancel(data.order_id)} className=" bg-red-600 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-500 rounded">ยกเลิก</button>}

                                                            {data.order_status === 8 ?
                                                                <button onClick={() => popupSeeProduct(data.order_id)} className="bg-amber-400 hover:bg-amber-500 text-white font-bold py-1 px-2 border border-yellow-300 rounded">ดูรูป</button>
                                                                : <></>}

                                                            <button onClick={() => popupMyorder(data.order_id)} className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-1 px-2 border border-blue-500 rounded">รายละเอียด</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        )
                                    })}

                            </table>

                        </div>
                    </div>
                </div>
            )}

        </div>
    )
}

export default Mytable