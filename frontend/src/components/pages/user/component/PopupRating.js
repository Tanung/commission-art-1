import axios from "axios";
import { React, useState } from "react";

function PopupRating({ popupRating, setPopupRating, dataPainter, dataUser }) {
    const dropDownRating = [
        { score: "1" },
        { score: "2" },
        { score: "3" },
        { score: "4" },
        { score: "5" },
    ];
    const [dataRating, setDataRating] = useState('1');
    // console.log("dropDownRating: ", dropDownRating.map((data) => data.score))
    // console.log("rating: ", dataRating)
    // console.log("ID_user: ", dataUser.id)
    // console.log("ID_painter: ", dataPainter.painter_id)

    ///------------------ Popup cancel order ---------------------///
    const clickGiveRating = async (e) => {
        e.preventDefault()
        if (dataRating.rating === '') {
            e.preventDefault();
            alert("กรุณาให้คะแนน");
        } else {
            e.preventDefault();
            await axios.post(`http://localhost:8000/user/click/rating`, {
                user_id: dataUser.id,
                painter_id: dataPainter.painter_id,
                rating: dataRating,
            })
                .then((res) => {
                    // console.log("Response:", res.data);
                    setPopupRating(!popupRating)
                    window.location.reload();
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }

    //----------------------------------- รับข้อมูลจาก input --------------------------------------///
    const onChangeRating = (e) => {
        e.preventDefault();
        setDataRating(e.target.value)
    }

    return (
        <div className="justify-center items-center text-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            <div className="bg-white grid rounded py-4 h-auto w-[23%] text-black">
                <div className="row-start-1 grid items-center">
                    <div className="text-2xl font-medium text-red-600">
                        ให้คะแนน
                    </div>

                </div>
                <div className="mt-4 space-y-4">
                    {/* <input onChange={onChangeRating} type="text" name="rating" value={dataRating.rating} /> */}
                    <select onChange={onChangeRating}>
                        {/* <option value="" disabled hidden>
                            {dataStatus.status}
                        </option> */}
                        {dropDownRating.map((data, key) => {
                            return (
                                <option key={key} name="rating" value={data.score}>{data.score}</option>
                            )
                        })}
                    </select>
                </div>

                <div className='flex items-end mt-4 justify-center space-x-5'>
                    <div className="grid font-skvsb">
                        <button
                            type="submit"
                            className="bg-lime-600 text-white rounded w-24 h-8"
                            onClick={clickGiveRating}
                        >
                            ตกลง
                        </button>
                    </div>
                    <div className="grid font-skvsb">
                        <button
                            type="submit"
                            className="bg-gray-400 text-white rounded w-24 h-8"
                            onClick={() => setPopupRating(!popupRating)}
                        >
                            ยกเลิก
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PopupRating