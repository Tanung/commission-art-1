import { React, useEffect, useState } from "react";
import axios from "axios";
import Loading from "react-loading";
import { saveAs } from "file-saver"

function PopupProduct({ popupProduct, setPopupProduct, id_order }) {

    const [dataProduct, setDataProduct] = useState('');
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/user/view/product/${id_order}`)
                .then((resp) => {
                    // console.log("resp", resp.data)
                    setDataProduct(resp.data.product);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [id_order]);

    ////---------------------- Function Download image --------------------//////
    const downloadImage = () => {
        saveAs(dataProduct.product_img, "DownloadFile.jpeg");
    };

    return (
        <div className="justify-center py-5 text-center flex overflow-x-hidden overflow-y-scroll fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">

            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className="bg-gray-100 px-8 py-5 rounded h-max w-[70%] text-black">
                    {/* <form onSubmit={Submit}> */}

                    <div className="grid grid-cols-3 text-2xl font-medium text-[#3F91DD]">
                        {/* <div className="col-start-2 text-center">รายละเอียดรูปภาพ</div> */}
                        <button onClick={() => setPopupProduct(!popupProduct)} className="col-start-3 font-semibold text-3xl text-red-500 text-right">x</button>
                    </div>
                    <div className="flex justify-center mt-5">
                        <div className="h-[60%] w-auto border-2 border-cyan-600">
                            <img className=" h-full w-full rounded-md" src={dataProduct.product_img} alt="" />
                        </div>
                    </div>
                    <button onClick={downloadImage} className="mt-5 h-10 w-36 bg-lime-600 text-lg text-white rounded">
                        ดาวโหลดรูปภาพ
                    </button>
                </div>
            )}

        </div>
    )
}

export default PopupProduct