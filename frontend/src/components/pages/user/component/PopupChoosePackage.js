import { React, useEffect, useState } from "react";
import axios from "axios";
import Loading from "react-loading";

function PopupChoosePackage({ popupChoosePackage, setPopupChoosePackage, id_package, dataUser, dataPainter }) {
    const [detailOrder, setDetailOrder] = useState({
        order_detail: "",
    })
    const [dataPackage, setDataPackage] = useState('');
    // const [dataPainter, setDataPainter] = useState('');
    const [loading, setLoading] = useState(true);
    const [image, setImage] = useState("");
    // const [message, setMessage] = useState();
    // console.log("Choose Package:", id_package)
    // console.log("dataPackage:", dataPackage)
    // console.log("dataPainter:", dataPainter)
    // console.log("dataUser:", dataUser)

    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/user/choose/package/${id_package}`)
                .then((resp) => {
                    // console.log(resp.data.dataPainter)
                    setDataPackage(resp.data.package);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [id_package]);

    //----------------------------------- รับข้อมูลจาก input --------------------------------------///
    const onChangeDetail = (e) => {
        e.preventDefault();
        const order = { ...detailOrder };
        order[e.target.name] = e.target.value;
        setDetailOrder(order)
    }

    //----------------------------------- แปลงรูปเปลี่ยนเป็น base64 --------------------------------------///
    const Imagebase64 = (e) => {
        e.preventDefault()
        const file = e.target.files[0];
        const reader = new FileReader();
        // console.log("file:", file)

        reader.onloadend = () => {
            setImage(reader.result.toString());
        };
        reader.readAsDataURL(file)
    }

    const removeImage = () => {
        setImage('');
    }

    //------------------ ส่งข้อมูลไป backend ---------------------///
    const Submit = async (e) => {
        e.preventDefault();
        if (image.length === 0) {
            e.preventDefault();
            alert("กรุณาเลือกรูปภาพ");
        } else if (detailOrder.order_detail === '') {
            e.preventDefault();
            alert("กรุณากรอกข้อมูลเพิ่มเติม");
        } else {
            // e.preventDefault();
            axios.post("http://localhost:8000/user/order", {
                user_id: dataUser.id,
                painter_id: dataPackage.painter_id,
                package_id: dataPackage.package_id,
                order_image: image,
                order_detail: detailOrder.order_detail,
                user_name: dataUser.name,
                user_phone: dataUser.phone,
                user_email: dataUser.email,
                painter_name: dataPainter.painter_name,
                painter_phone: dataPainter.painter_phone,
                painter_style: dataPainter.painter_style,
                painter_email: dataPainter.painter_email,
                package_name: dataPackage.package_name,
                package_character: dataPackage.package_character,
                package_bgcolor: dataPackage.package_bgcolor,
                package_size: dataPackage.package_size,
                package_resolution: dataPackage.package_resolution,
                package_file: dataPackage.package_file,
                package_price: dataPackage.package_price,
            })
                .then((res) => {
                    // console.log("Response:", res.data);
                    setPopupChoosePackage(!popupChoosePackage)
                    // window.location.reload();
                })
                .catch((error) => {
                    console.log(error);
                });
            }
        }



        return (
            <div className="justify-center py-5 text-center flex overflow-x-hidden overflow-y-scroll fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">

                {loading === true ? (
                    <div className="grid">
                        <div className="grid justify-items-center mt-80">
                            <Loading
                                type="bubbles"
                                color="#0000FF"
                                height={200}
                                width={100}
                            />
                        </div>
                    </div>
                ) : (
                    <div className="bg-white px-8 py-5 rounded h-max w-[70%] text-black">
                        <form onSubmit={Submit}>

                            <div className="grid grid-cols-3 text-2xl font-medium text-[#3F91DD]">
                                <div className="col-start-2 text-center">รายละเอียดรูปภาพ</div>
                                <button onClick={() => setPopupChoosePackage(!popupChoosePackage)} className="col-start-3 font-semibold text-3xl text-red-500 text-right">x</button>
                            </div>

                            <div className="grid grid-cols-3 mt-7 text-left">
                                <div className="">ชื่อผู้จอง: {dataUser.name}</div>
                                <div className="">เบอร์โทร: {dataUser.phone}</div>
                                <div className="">อีเมล์: {dataUser.email}</div>
                            </div>

                            <div className="grid grid-rows-8 mt-5 text-left">
                                <div className="row-start-1 grid grid-cols-7">
                                    <div className="">แพ็กเกจที่เลือก: </div>
                                    <div className="col-start-2 col-span-5">แพ็กเกจ: {dataPackage.package_name}</div>
                                </div>
                                <div className="row-start-2 grid grid-cols-7">
                                    <div className="col-start-2 col-span-5">ตัวละคร: {dataPackage.package_character}</div>
                                </div>
                                {/* <div className="row-start-3 grid grid-cols-7">
                                    <div className="col-start-2 col-span-5">สีตัวละคร: #######วาดภาพแนว miminal######</div>
                                </div> */}
                                <div className="row-start-4 grid grid-cols-7">
                                    <div className="col-start-2 col-span-5">สีพื้นหลัง: {dataPackage.package_bgcolor}</div>
                                </div>
                                <div className="row-start-5 grid grid-cols-7">
                                    <div className="col-start-2 col-span-5">ความละเอียดภาพ: {dataPackage.package_resolution}</div>
                                </div>
                                <div className="row-start-6 grid grid-cols-7">
                                    <div className="col-start-2 col-span-5">ขนาดรูปภาพ: {dataPackage.package_size}</div>
                                </div>
                                <div className="row-start-7 grid grid-cols-7">
                                    <div className="col-start-2 col-span-5">ส่งงานเป็นไฟล์: {dataPackage.package_file}</div>
                                </div>
                                <div className="row-start-8 grid grid-cols-7">
                                    <div className="col-start-2 col-span-5">ราคา: {dataPackage.package_price} บาท</div>
                                </div>
                            </div>

                            <div className="text-left mt-5">
                                เลือกรูปภาพที่ต้องการให้วาด:
                            </div>

                            <div className="flex justify-center mt-5 items-center px-3">
                                <div className="rounded-lg shadow-xl bg-gray-50 md:w-1/2 w-[360px]">
                                    <div className="m-4">
                                        {/* <span className="flex justify-center items-center text-[12px] mb-1 text-red-500">{message}</span> */}
                                        <div className="flex items-center justify-center w-full">
                                            <label className="flex cursor-pointer flex-col w-full h-32 border-2 rounded-md border-dashed hover:bg-gray-100 hover:border-gray-300">
                                                <div className="flex flex-col items-center justify-center pt-7">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                        className="w-12 h-12 text-gray-400 group-hover:text-gray-600" viewBox="0 0 20 20"
                                                        fill="currentColor">
                                                        <path filerule="evenodd"
                                                            d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z"
                                                            clipRule="evenodd" />
                                                    </svg>
                                                    <p className="pt-1 text-sm tracking-wider text-gray-400 group-hover:text-gray-600">
                                                        Select a photo</p>
                                                </div>
                                                <input type="file" onChange={Imagebase64} className="opacity-0" multiple="multiple" name="files[]" />
                                            </label>
                                        </div>
                                        <div className="flex flex-wrap gap-2 mt-2">

                                            {/* {image.map((file, key) => {
                                        return (
                                            <div key={key} className="overflow-hidden relative">
                                                <div onClick={() => { removeImage(file.name) }} className="mdi mdi-close absolute right-1 hover:text-red-500 cursor-pointer">x</div>
                                                <img className="h-20 w-20 rounded-md" src={URL.createObjectURL(file)} alt="" />
                                            </div>
                                        )
                                    })} */}

                                            {image.length !== 0 ?
                                                (<div className="overflow-hidden relative">
                                                    <div onClick={removeImage} className="mdi mdi-close absolute right-1 hover:text-red-500 cursor-pointer">x</div>
                                                    <img className="h-20 w-20 rounded-md" src={image} alt="" />
                                                </div>) :
                                                null
                                            }

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="text-left mt-5">
                                ระบุรายละเอียดรูปวาด: เช่น สีตัวละคร, สีพื้นหลัง, ขนาดรูปภาพ, ไฟล์รูปภาพ
                            </div>

                            <div>
                                <textarea
                                    name="order_detail"
                                    value={detailOrder.order_detail}
                                    onChange={onChangeDetail}
                                    className="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
                                    id="exampleFormControlTextarea1"
                                    rows="3"
                                    placeholder="Your message"
                                ></textarea>
                            </div>
                            {/* <div className="text-left mt-5">
                                นำรูปภาพไปใช้ทำอะไร
                            </div>

                            <div>
                                <textarea
                                    className="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
                                    id="exampleFormControlTextarea1"
                                    rows="3"
                                    placeholder="Your message"
                                ></textarea>
                            </div> */}

                            <button type="submit" className="mt-8 h-9 w-24 rounded bg-lime-400 hover:bg-lime-500">
                                สั่งจอง
                            </button>
                        </form>
                    </div>
                )}

            </div>
        )
    }

    export default PopupChoosePackage