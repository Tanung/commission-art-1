import axios from "axios";
import { React } from "react";

function PopupCancel({ popupCancel, setPopupCancel, id_order }) {

    ///------------------ Popup cancel order ---------------------///
    const clickCancel = async (e) => {
        e.preventDefault()
        await axios
            .put(`http://localhost:8000/user/cancel/order/${id_order}`, {
                order_status: 3,
            })
            .then((resp) => {
                // console.log("Resp:", resp.data);
                setPopupCancel(!popupCancel)
                window.location.reload();
            })
            .catch((err) => console.log(err));
    }

    return (
        <div className="justify-center items-center text-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            <div className="bg-white grid grid-rows-3 rounded py-4 h-40 w-[23%] text-black">
                <div className="row-start-1">
                    <div className="text-2xl font-medium text-red-600">
                        <div>Are you sure?</div>
                    </div>

                </div>
                <div className="row-start-2 ">
                    <div className="">
                        <div>Do you want to cancel?</div>
                    </div>

                </div>
                <div className='row-start-3 flex items-end justify-center space-x-5'>
                    <div className="grid font-skvsb">
                        <button
                            type="submit"
                            className="bg-red-600 text-white rounded w-24 h-8"
                            onClick={clickCancel}
                        >
                            Yes
                        </button>
                    </div>
                    <div className="grid font-skvsb">
                        <button
                            type="submit"
                            className="bg-gray-400 text-white rounded w-24 h-8"
                            onClick={() => setPopupCancel(!popupCancel)}
                        >
                            No
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PopupCancel