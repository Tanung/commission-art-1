import React from 'react'
import { useLocation } from "react-router-dom";


function NavbarUser() {
    const { pathname } = useLocation();
    const HeaderItems = [
        { name: 'ข้อมูลการจอง', href: '/Home/Mytable' },
        // { name: 'รอคิว', href: '/Home/Queue' },
        // { name: 'กำลังวาด', href: '/Home/Progress' },
        // { name: 'สำเร็จ', href: '/Home/Success' },
        // { name: 'ยกเลิกการจอง', href: '/Home/Cancel' },
    ];
    HeaderItems.filter(data => data.href === pathname).map(data2 => data2.current = true);

    function classNames(...classes) {
        return classes.filter(Boolean).join(' ')
    }

    return (
        <div className='grid items-center rounded bg-white h-14'>
            <ul className="flex mx-3">
                {HeaderItems.map((data, index) => (
                    <li className="mr-3"  aria-current={data.current ? 'page' : undefined} key={index}>
                        <a href={data.href} aria-current={data.current ? 'page' : undefined} className={classNames(data.current ? 'inline-block border border-blue-500 rounded py-1 px-3 bg-blue-500 text-white' : 'inline-block border border-white rounded hover:border-gray-200 text-blue-500 hover:bg-gray-200 py-1 px-3')}>{data.name}</a>
                    </li>
                ))}
            </ul>
        </div>

    )
}

export default NavbarUser