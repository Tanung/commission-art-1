import axios from "axios";
import { useParams } from "react-router-dom";
import Loading from "react-loading";
import { React, useEffect, useState } from "react";

import HeaderUser from "../../component/HeaderUser"

function Profile() {
    const [Mydata, setMyData] = useState('');
    // console.log("data:", Mydata)
    const [loading, setLoading] = useState(true);
    const { id_user } = useParams();

    //---------------- api profile user ---------------//
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/user/profile/${id_user}`)
                .then((resp) => {
                    setMyData(resp.data[0]);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [id_user]);

    //---------------- OnChange ---------------//
    const onChangeUpdate = (e) => {
        e.preventDefault();
        const newData = { ...Mydata };
        newData[e.target.name] = e.target.value;
        setMyData(newData)
    }

    //----------------------------------- รับค่า Number Phone จาก input --------------------------------------///
    const onChangePhoneNumber = (e) => {
        e.preventDefault();
        const numberPhone = { ...Mydata };
        numberPhone[e.target.name] = e.target.value.slice(0, 10);
        setMyData(numberPhone)
    }


    //---------------- onSubmit ---------------//
    const editSubmit = (e) => {
        e.preventDefault();
        axios.put(`http://localhost:8000/user/edit/profile/${id_user}`, {
            name: Mydata.name,
            username: Mydata.username,
            email: Mydata.email,
            phone: Mydata.phone
        })

            .then((res) => {
                console.log(res.data);
                alert("แก้ไขสำเร็จ");
                // navigate("/Admin");
            })
            .catch((error) => {
                console.log(error);
            });
    }

    return (
        <div className="bg-grey-lighter h-screen flex flex-col">

            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <>
                    <HeaderUser />
                    <div className="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
                        <div className="bg-white px-6 py-8 rounded shadow-md text-black w-full">
                            <form onSubmit={editSubmit}>
                                <h1 className="mb-8 text-3xl text-center">Profile</h1>
                                <input
                                    type="text"
                                    className="block border border-grey-light w-full p-3 rounded mb-4"
                                    name="name"
                                    value={Mydata.name}
                                    onChange={onChangeUpdate}
                                />

                                <input
                                    type="text"
                                    className="block border border-grey-light w-full p-3 rounded mb-4"
                                    name="username"
                                    value={Mydata.username}
                                    onChange={onChangeUpdate}
                                />

                                <input
                                    type="text"
                                    className="block border border-grey-light w-full p-3 rounded mb-4"
                                    name="email"
                                    value={Mydata.email}
                                    onChange={onChangeUpdate}
                                />

                                <input
                                    type="number"
                                    className="block border border-grey-light w-full p-3 rounded mb-4"
                                    name="phone"
                                    value={Mydata.phone}
                                    onChange={onChangePhoneNumber}
                                />

                                <button
                                    type="submit"
                                    className="w-full text-center py-3 bg-green-400 rounded text-white hover:bg-green-500 focus:outline-none my-1"
                                >Edit profile</button>
                            </form>
                        </div>
                    </div>
                </>
            )}

        </div>
    )
}

export default Profile