import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

import Header from "../../component/Header"

function RegisterPainter() {
    const navigate = useNavigate();

    //-----------------------------------  useState --------------------------------------///
    const [createPainter, setCreatePainter] = useState({
        name: "",
        username: "",
        email: "",
        phone: "",
        style: "",
        password: "",
        confirm_password: "",
    })
    const [image, setImage] = useState('');
    // console.log("Data:", createPainter)

    const stylePainter = [
        "Anime",
        "Minimal",
        "Semi-realistic",
        "Chibi"
    ];

    //----------------------------------- รับข้อมูลจาก input --------------------------------------///
    const onChangePainter = (e) => {
        e.preventDefault();
        const newPainter = { ...createPainter };
        newPainter[e.target.name] = e.target.value;
        setCreatePainter(newPainter)
    }

    //----------------------------------- รับค่า Number Phone จาก input --------------------------------------///
    const onChangePhoneNumber = (e) => {
        e.preventDefault();
        const numberPhone = { ...createPainter };
        numberPhone[e.target.name] = e.target.value.slice(0, 10);
        setCreatePainter(numberPhone)
    }

    //----------------------------------- แปลงรูปเปลี่ยนเป็น base64 --------------------------------------///
    const Imagebase64 = (e) => {
        e.preventDefault()
        const file = e.target.files[0];
        const reader = new FileReader();

        reader.onloadend = () => {
            setImage(reader.result.toString());
        };
        reader.readAsDataURL(file)
    }

    const removeImage = () => {
        setImage('');
    }

    //------------------ ส่งข้อมูลไป backend ---------------------///
    const Submit = async (e) => {
        e.preventDefault();
        if (createPainter.name === '') {
            e.preventDefault();
            alert("กรุณากรอกชื่อ");
        } else if (createPainter.username === '') {
            e.preventDefault();
            alert("กรุณากรอกชื่อผู้ใช้");
        } else if (createPainter.email === '') {
            e.preventDefault();
            alert("กรุณากรอกอีเมล์");
        } else if (createPainter.phone === '') {
            e.preventDefault();
            alert("กรุณากรอกเบอร์โทร");
        } else if (createPainter.style === '') {
            e.preventDefault();
            alert("กรุณาเลือกสไตล์วาดรูป");
        } else if (createPainter.password === '') {
            e.preventDefault();
            alert("กรุณากรอกรหัสผ่าน");
        } else if (createPainter.confirm_password === '') {
            e.preventDefault();
            alert("กรุณากรอกยืนยันรหัสผ่าน");
        } else if (createPainter.password !== createPainter.confirm_password) {
            e.preventDefault();
            alert("รหัสผ่านไม่ตรงกัน");
        } else if (image.length === 0) {
            e.preventDefault();
            alert("กรุณาเลือกรูปภาพโปรไฟล์");
        } else {
            e.preventDefault();
            // console.log("ผ่านๆๆๆๆ")
            axios.post("http://localhost:8000/painter/register", {
                painter_name: createPainter.name,
                painter_username: createPainter.username,
                painter_email: createPainter.email,
                painter_phone: createPainter.phone,
                painter_style: createPainter.style,
                painter_password: createPainter.password,
                painter_image: image,
            })
                .then((res) => {
                    console.log("Response:", res.data);
                    alert("Create Account Successfull!!");
                    navigate("/LoginPainter");
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }


    return (
        <div className="bg-grey-lighter min-h-screen flex flex-col">
            <Header />
            <div className="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
                <div className="bg-white mt-7 mb-5 px-6 py-8 rounded shadow-md text-black w-full">
                    <form onSubmit={Submit}>
                        <h1 className="mb-8 text-3xl text-center">Sign up</h1>
                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="name"
                            value={createPainter.name}
                            onChange={onChangePainter}
                            placeholder="Name" />

                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="username"
                            value={createPainter.username}
                            onChange={onChangePainter}
                            placeholder="Username" />

                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="email"
                            value={createPainter.email}
                            onChange={onChangePainter}
                            placeholder="Email" />

                        <input
                            type="number"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="phone"
                            value={createPainter.phone}
                            onChange={onChangePhoneNumber}
                            placeholder="Phone Number" />

                        <input
                            type="password"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="password"
                            value={createPainter.password}
                            onChange={onChangePainter}
                            placeholder="Password" />

                        <input
                            type="password"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="confirm_password"
                            value={createPainter.confirm_password}
                            onChange={onChangePainter}
                            placeholder="Confirm Password" />

                        <div className='flex mb-4 space-x-2'>
                            <h1 className='text-lg font-medium'>Style: </h1>
                            <div className='space-y-2'>
                                {
                                    stylePainter.map((style, key) => (
                                        <div key={key} className='mt-0.5'>
                                            <input
                                                type="radio"
                                                // className="block border border-grey-light w-full p-3 rounded mb-4"
                                                name="style"
                                                value={style}
                                                onChange={onChangePainter} /> {style}
                                        </div>
                                    ))
                                }
                            </div>
                        </div>

                        <div className='mb-4'>
                            <input type="file" onChange={(e) => Imagebase64(e)} />
                            {image.length !== 0 ?
                                (<div className="overflow-hidden relative mt-2">
                                    <div onClick={removeImage} className="mdi mdi-close absolute ml-[67px] hover:text-red-500 cursor-pointer">x</div>
                                    <img className="h-20 w-20 rounded-md" src={image} alt="" />
                                </div>) :
                                null
                            }
                        </div>

                        <button
                            type="submit"
                            className="w-full text-center py-3 bg-green-400 rounded text-white hover:bg-green-500 focus:outline-none my-1"
                        >Create Account</button>
                    </form>
                </div>

                <div className="flex space-x-2 text-grey-dark mt-6">
                    <div>
                        Already have an account?
                    </div>
                    <a className="no-underline text-red-600  hover:text-red-700 focus:text-red-700 border-b border-blue text-blue" href="/LoginPainter">
                        Log in
                    </a>.
                </div>
            </div>
        </div>
    )
}

export default RegisterPainter