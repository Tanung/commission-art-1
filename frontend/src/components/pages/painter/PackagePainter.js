import { React, useState, useEffect } from 'react'
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Loading from "react-loading";
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import HeaderPainter from "../../component/HeaderPainter"
import NavbarPainter from '../painter/NavbarPainter';
import PopupAddpackage from '../painter/component/PopupAddpackage';
import PopupDeletepackage from './component/PopupDeletepackage';
import PopupEditpackage from './component/PopupEditpackage';

function PackagePainter() {
    const navigate = useNavigate();
    const [dataPainter, setDataPainter] = useState('');
    const [dataPackage, setDataPackage] = useState([]);
    const [popupPackage, setPopupPackage] = useState(true)
    const [popupDelete, setPopupDelete] = useState(true)
    const [popupEdit, setPopupEdit] = useState(true)
    const [id_package, setId_package] = useState('')
    const [loading, setLoading] = useState(true);
    const token = localStorage.getItem('TokenPainter')
    // console.log("data:", dataPainter)
    // console.log("packages:", dataPackage)

    //---------------- data, packages painter ---------------//
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/MydataPainter/${token}`)
                .then((resp) => {
                    setDataPainter(resp.data.data[0]);
                    setDataPackage(resp.data.packages);
                    setLoading(false);
                    // console.log("data: ", resp.data[0].painter_id)
                })
                .catch((err) => console.log(err));
        })();
    }, [token]);

    ///------------------ Popup Add package ---------------------///
    const popupAddPackage = () => {
        setPopupPackage(!popupPackage)
    }

    ///------------------ Popup delete package ---------------------///
    const popupDeletePackage = (id) => {
        setId_package(id)
        setPopupDelete(!popupDelete)
    }

    ///------------------ Popup edit package ---------------------///
    const popupEditPackage = (id) => {
        setId_package(id)
        setPopupEdit(!popupEdit)
    }

    //---------------- Nevigate params profile user ---------------//
    const painterClickProfile = (data) => {
        navigate(`/Profile/${data}`);
        // console.log("check:", data)
    };

    return (

        <div className='bg-gray-200 w-full h-screen'>
            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className='bg-gray-200'>

                    {!popupPackage ?
                        <PopupAddpackage popupPackage={popupPackage} setPopupPackage={setPopupPackage} ID_Painter={dataPainter.painter_id} />
                        : null
                    }

                    {!popupDelete ?
                        <PopupDeletepackage popupDelete={popupDelete} setPopupDelete={setPopupDelete} id_package={id_package} />
                        : null
                    }

                    {!popupEdit ?
                        <PopupEditpackage popupEdit={popupEdit} setPopupEdit={setPopupEdit} id_package={id_package} />
                        : null
                    }

                    <HeaderPainter painterClickProfile={painterClickProfile} />
                    {/* <!-- component --> */}
                    <div className='px-5 py-5 mx-auto container'>

                        <NavbarPainter />

                        <div className='mt-5'>

                            <div className='flex justify-between py-7 mx-14'>
                                <p className='font-bold text-[#615F5F] text-2xl'>แพ็กเกจ</p>
                                <button onClick={popupAddPackage} className='bg-lime-600 rounded-md w-[10%] font-medium text-[16px] text-white hover:bg-lime-500'>เพิ่มแพ็กเกจ</button>
                            </div>

                            {dataPackage.length === 0 ? (
                                <div className="flex h-full justify-center mt-32 space-x-2 font-skvb">
                                    <FontAwesomeIcon
                                        icon={faTriangleExclamation}
                                        className="text-yellow-300 mt-1 w-7 h-7"
                                    />
                                    <h1 className="mt-2">No Data Found</h1>
                                </div>
                            ) : dataPackage.map((data, key) => {
                                return (
                                    <div key={key} className='px-[20%]'>
                                        <div className="bg-white space-y-2 w-full mb-7 py-2.5 px-7 border-2 rounded-lg">
                                            <div className="flex justify-between">
                                                <div className="flex items-center">
                                                    <h1 className="font-semibold text-[17px]">แพ็กเกจ: </h1>
                                                    <p className="ml-2">{data.package_name}</p>
                                                </div>
                                                <div className="flex space-x-2 h-8 w-24 rounded-lg font-semibold text-sm">
                                                    <button onClick={() => popupEditPackage(data.package_id)} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 border border-blue-500 rounded">Edit</button>
                                                    <button onClick={() => popupDeletePackage(data.package_id)} className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-500 rounded">Delete</button>
                                                </div>
                                            </div>
                                            <div className="flex items-center">
                                                <h1 className="font-semibold text-[17px]">ตัวละคร: </h1>
                                                <p className="ml-2">{data.package_character}</p>
                                            </div>
                                            <div className="flex items-center">
                                                <h1 className="font-semibold text-[17px]">สีตัวละคร: </h1>
                                                <p className="ml-2">##########เลือกได้########</p>
                                            </div>
                                            <div className="flex items-center">
                                                <h1 className="font-semibold text-[17px]">สีพื้นหลัง: </h1>
                                                <p className="ml-2">{data.package_bgcolor}</p>
                                            </div>
                                            <div className="flex items-center">
                                                <h1 className="font-semibold text-[17px]">ขนาดรูปภาพ: </h1>
                                                <p className="ml-2">{data.package_size}</p>
                                            </div>
                                            <div className="flex items-center">
                                                <h1 className="font-semibold text-[17px]">ความละเอียดภาพ: </h1>
                                                <p className="ml-2">{data.package_resolution}</p>
                                            </div>
                                            <div className="flex justify-between">
                                                <div className="flex items-center">
                                                    <h1 className="font-semibold text-[17px]">ส่งงานเป็นไฟล์: </h1>
                                                    <p className="ml-2">{data.package_file}</p>
                                                </div>
                                                <div className="flex items-center">
                                                    <h1 className="font-semibold text-[17px]">ราคา: </h1>
                                                    <p className="ml-2">{data.package_price} บาท</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}


                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}

export default PackagePainter