import { React, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Loading from "react-loading";
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// import Header from "../../component/Header"
import HeaderPainter from "../../component/HeaderPainter"
import NavbarPainter from '../painter/NavbarPainter';
import PopupStatus from "./component/PopupStatus";
import PopupSubmitWork from "./component/PopupSubmitWork";
import PopupProduct from "../user/component/PopupProduct";
import PopupDetail from "./component/PopupDetail"

function QueuePainter() {
    const navigate = useNavigate();
    const [dataOrder, setDataOrder] = useState([])
    const [dataStatus, setDataStatus] = useState('')
    const [dropdownStatus, setDropdownStatus] = useState([])
    const [popupStatus, setPopupStatus] = useState(true)
    const [popupSubmitWork, setPopupSubmitWork] = useState(true)
    const [popupDetail, setPopupDetail] = useState(true)
    const [popupProduct, setPopupProduct] = useState(true)
    const [id_order, setId_order] = useState([]);
    const [loading, setLoading] = useState(true);
    const token = localStorage.getItem('TokenPainter')
    // console.log("dataOrder:", dataOrder)
    // console.log("Check Status: ", dataStatus)
    // console.log("Status:", dropdownStatus)

    //----------------------------------- get order user and all status --------------------------------------///
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/painter/see/All_queue/${token}`)
                .then((resp) => {
                    // console.log("resp:", resp.data.users)
                    setDataOrder(resp.data.orders);
                    setDropdownStatus(resp.data.status);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [token]);

    //----------------------------------- Change Status --------------------------------------///
    const handleChangeStatus = (e) => {
        e.preventDefault();
        setDataStatus(e.target.value)
    }

    //---------------- Nevigate params profile user ---------------//
    const painterClickProfile = (data) => {
        navigate(`/Profile/${data}`);
    };

    ///------------------ Popup State ---------------------///
    const popup_Status = (id) => {
        setId_order(id)
        setPopupStatus(!popupStatus)
    }

    ///------------------ Popup submit work ---------------------///
    const popup_SubmitWork = (id) => {
        setId_order(id)
        setPopupSubmitWork(!popupSubmitWork)
    }

    ///------------------ Popup see product ---------------------///
    const popupSeeProduct = (id) => {
        setId_order(id)
        setPopupProduct(!popupProduct)
    }

    ///------------------ Popup Detail ---------------------///
    const popup_Detail = (id) => {
        setId_order(id)
        setPopupDetail(!popupDetail)
    }

    return (

        <div className='bg-gray-200 w-full h-screen'>
            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className='bg-gray-200'>
                    <HeaderPainter painterClickProfile={painterClickProfile} />

                    {!popupStatus ?
                        <PopupStatus popupStatus={popupStatus} setPopupStatus={setPopupStatus} id_order={id_order} dataStatus={dataStatus} />
                        : null
                    }

                    {!popupSubmitWork ?
                        <PopupSubmitWork popupSubmitWork={popupSubmitWork} setPopupSubmitWork={setPopupSubmitWork} id_order={id_order} />
                        : null
                    }

                    {!popupDetail ?
                        <PopupDetail popupDetail={popupDetail} setPopupDetail={setPopupDetail} id_order={id_order} />
                        : null
                    }

                    {!popupProduct ?
                        <PopupProduct popupProduct={popupProduct} setPopupProduct={setPopupProduct} id_order={id_order} />
                        : null
                    }

                    <div className='px-5 py-5 mx-auto container'>

                        <NavbarPainter />

                        <div className='mt-5'>

                            <div className='font-bold text-[#615F5F] py-7 text-2xl ml-14'>
                                คิวทั้งหมด
                            </div>

                            <table className="min-w-full text-center border-collapse block md:table">
                                <thead className="block md:table-header-group">
                                    <tr className="border border-grey-500 md:border-none block md:table-row absolute -top-full md:top-auto -left-full md:left-auto  md:relative ">
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ลำดับ</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ชื่อผู้จอง</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">อีเมล์</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">วันที่ได้คิว</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">วันส่งงาน</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">สถานะ</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">Actions</th>
                                    </tr>
                                </thead>

                                {dataOrder.length === 0 ? (
                                    <tbody className="flex h-full justify-center mt-32 space-x-2 font-skvb">
                                        <tr className=''>
                                            <td>
                                                <FontAwesomeIcon
                                                    icon={faTriangleExclamation}
                                                    className="text-yellow-300 mt-1 w-7 h-7"
                                                />
                                                <h1 className="mt-2">No Data Found</h1>
                                            </td>
                                        </tr>
                                    </tbody>
                                ) : dataOrder.map((data, key) => {
                                    return (
                                        <tbody key={key} className="bg-white block md:table-row-group">
                                            <tr className="border border-grey-500 md:border-none block md:table-row">
                                                <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold"></span>{key + 1}</td>
                                                <td className="w-[20%] p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">ชื่อผู้จอง</span>{data.user_name}</td>
                                                <td className="w-[20%] p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">อีเมล์</span>{data.user_email}</td>
                                                <td className="w-[15%] p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">วันที่ได้คิว</span>{data.order_dateQ}</td>
                                                <td className="w-[15%] p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">วันส่งงาน</span>{data.order_deadline}</td>
                                                <td className="w-[15%] p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">สถานะ</span>

                                                    {dropdownStatus.map((dataStatus, key) => {
                                                        return (
                                                            <div key={key}>
                                                                {dataStatus.status_id === data.order_status ?
                                                                    <select onChange={handleChangeStatus} defaultValue="">
                                                                        <option value="" disabled hidden>
                                                                            {dataStatus.status}
                                                                        </option>
                                                                        {dropdownStatus.map((AllStatus, key) => {
                                                                            return (
                                                                                <option key={key} name="order_status" value={AllStatus.status_id}>{AllStatus.status}</option>
                                                                            )
                                                                        })}
                                                                    </select> : null
                                                                }
                                                            </div>
                                                        )
                                                    })}
                                                </td>
                                                <td className="p-2 md:border md:border-grey-500 block md:table-cell">
                                                    <span className="inline-block w-1/3 md:hidden font-bold">Actions</span>
                                                    <div className='flex space-x-1 items-center justify-center'>
                                                        {data.order_status === 7 ?
                                                            <button onClick={() => popup_SubmitWork(data.order_id)} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 border border-blue-500 rounded">ส่งงาน</button>
                                                            : <></>}

                                                        {data.order_status === 8 ?
                                                            <button onClick={() => popupSeeProduct(data.order_id)} className="bg-amber-400 hover:bg-amber-500 text-white font-bold py-1 px-2 border border-yellow-300 rounded">ดูรูป</button>
                                                            : <button onClick={() => popup_Status(data.order_id)} type="submit" className="bg-lime-500 hover:bg-lime-600 text-white font-bold py-1 px-2 border border-lime-400 rounded">ยืนยัน</button>}
                                                        
                                                        <button onClick={() => popup_Detail(data.order_id)} className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-500 rounded ">รายละเอียด</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    )
                                })}

                            </table>
                        </div>
                    </div>
                </div>
            )}
        </div>
    )

}

export default QueuePainter