import axios from 'axios';
import { useState } from 'react'
// import { useNavigate } from "react-router-dom";
// var FormData = require('form-data')

function TestImage555() {
    // const navigate = useNavigate();

    // const [files, setFile] = useState('');
    // const [message, setMessage] = useState();
    // console.log(files)

    // //------------------ Choose image ---------------------///
    // const handleFile = (e) => {
    //     setMessage("");
    //     let file = e.target.files;

    //     for (let i = 0; i < file.length; i++) {
    //         const fileType = file[i]['type'];
    //         const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
    //         if (validImageTypes.includes(fileType)) {
    //             setFile([...files, file[i]]);
    //         } else {
    //             setMessage("only images accepted");
    //         }

    //     }
    // };
    // const removeImage = (i) => {
    //     setFile(files.filter(x => x.name !== i));
    // }

    // //------------------ Submit Image ---------------------///
    const uploadImage = async (e) => {
        // e.preventDefault();
        // const formData = new FormData();
        // // formData.append("filename", files[0].name);
        // formData.append("File", files);
        // // formData.append('filesize', files.size);
        // // formData.append('filetype', files.type);
        // // Array.from(files).forEach(image => {
        // //     formData.append("file", image);
        // // });
        // // console.log(formData)

        // axios.post("http://localhost:8000/upload", formData)
        //     .then((res) => {
        //         // const responses = res.json()
        //         console.log("Response:", res.data);
        //         //   alert("ข้อมูลถูกบันทึกแล้ว");
        //         //   navigate("/");
        //     })
        //     .catch((error) => {
        //         console.log(error);
        //     });
    }

    const [image, setImage] = useState("");
    console.log("ImageBase64:", image)

    const convert2base64 = (e) => {
        const file = e.target.files[0];
        const reader = new FileReader();
        console.log("file:", file)

        reader.onloadend = () => {
            setImage(reader.result.toString());
        };

        reader.readAsDataURL(file)
    }


    return (
        <div className="m-4">
            <form onSubmit={uploadImage}>

                {/* <span className="flex justify-center items-center text-[12px] mb-1 text-red-500">{message}</span> */}
                <div className="flex items-center justify-center w-full">
                    <label className="flex cursor-pointer flex-col w-full h-32 border-2 rounded-md border-dashed hover:bg-gray-100 hover:border-gray-300">
                        <div className="flex flex-col items-center justify-center pt-7">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                className="w-12 h-12 text-gray-400 group-hover:text-gray-600" viewBox="0 0 20 20"
                                fill="currentColor">
                                <path fillRule="evenodd"
                                    d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z"
                                    clipRule="evenodd" />
                            </svg>
                            <p className="pt-1 text-sm tracking-wider text-gray-400 group-hover:text-gray-600">
                                Select a photo</p>
                        </div>
                        <input type="file" onChange={(e) => convert2base64(e)} className="opacity-0" accept="image/*" multiple="multiple" name="files[]" />
                    </label>
                </div>
                <div className="flex flex-wrap gap-2 mt-2">

                    {/* {files.map((file, key) => {
                        return (
                            <div key={key} className="overflow-hidden relative">
                                <div onClick={() => { removeImage(file.name) }} className="mdi mdi-close absolute right-1 hover:text-red-500 cursor-pointer">x</div>
                                <img className="h-20 w-20 rounded-md" src={URL.createObjectURL(file)} alt="" />
                            </div>
                        )
                    })} */}

                </div>
                <button type='submit' className='h-8 w-16 rounded mt-4 bg-lime-600'>
                    Upload
                </button>
            </form>

            <div>
                {image ? (<img src={image} />) : null}
            </div>

        </div>



    )
}

export default TestImage555