import axios from "axios";
import { useParams } from "react-router-dom";
import Loading from "react-loading";
import { React, useEffect, useState } from "react";

import HeaderPainter from "../../component/HeaderPainter"

function ProfilePainter() {
    const [Mydata, setMyData] = useState('');
    // console.log("data:", Mydata)
    const [loading, setLoading] = useState(true);
    const { id_painter } = useParams();

    //---------------- api profile user ---------------//
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/painter/profile/${id_painter}`)
                .then((resp) => {
                    setMyData(resp.data[0]);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [id_painter]);

    //---------------- OnChange ---------------//
    const onChangeUpdate = (e) => {
        e.preventDefault();
        const newData = { ...Mydata };
        newData[e.target.name] = e.target.value;
        setMyData(newData)
    }

    //----------------------------------- รับค่า Number Phone จาก input --------------------------------------///
    const onChangePhoneNumber = (e) => {
        e.preventDefault();
        const numberPhone = { ...Mydata };
        numberPhone[e.target.name] = e.target.value.slice(0, 10);
        setMyData(numberPhone)
    }

    //---------------- onSubmit ---------------//
    const editSubmit = (e) => {
        e.preventDefault();
        axios.put(`http://localhost:8000/painter/edit/profile/${id_painter}`, {
            name: Mydata.painter_name,
            username: Mydata.painter_username,
            email: Mydata.painter_email,
            phone: Mydata.painter_phone
        })

            .then((res) => {
                console.log(res.data);
                alert("แก้ไขสำเร็จ");
                // navigate("/Admin");
            })
            .catch((error) => {
                console.log(error);
            });
    }

    return (
        <div className="bg-grey-lighter min-h-screen flex flex-col">
            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <>
                    <HeaderPainter />
                    <div className="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
                        <div className="bg-white px-6 py-8 rounded shadow-md text-black w-full">
                            <form onSubmit={editSubmit}>
                                <h1 className="mb-8 text-3xl text-center">Profile</h1>
                                <input
                                    type="text"
                                    className="block border border-grey-light w-full p-3 rounded mb-4"
                                    name="painter_name"
                                    value={Mydata.painter_name}
                                    onChange={onChangeUpdate}
                                />

                                <input
                                    type="text"
                                    className="block border border-grey-light w-full p-3 rounded mb-4"
                                    name="painter_username"
                                    value={Mydata.painter_username}
                                    onChange={onChangeUpdate}
                                />

                                <input
                                    type="text"
                                    className="block border border-grey-light w-full p-3 rounded mb-4"
                                    name="painter_email"
                                    value={Mydata.painter_email}
                                    onChange={onChangeUpdate}
                                />

                                <input
                                    type="number"
                                    className="block border border-grey-light w-full p-3 rounded mb-4"
                                    name="painter_phone"
                                    value={Mydata.painter_phone}
                                    onChange={onChangePhoneNumber}
                                />

                                <button
                                    type="submit"
                                    className="w-full text-center py-3 bg-green-400 rounded text-white hover:bg-green-500 focus:outline-none my-1"
                                >Edit profile</button>
                            </form>
                        </div>
                    </div>
                </>
            )}
        </div>
    )
}

export default ProfilePainter