import { React, useState } from 'react'
import { useNavigate, Navigate, useLocation } from "react-router-dom";
import axios from "axios";

import Header from "../../component/Header"

function LoginPainter() {
    const navigate = useNavigate();
    const location = useLocation();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const getToken = localStorage.getItem('TokenPainter')

    const login = (e) => {
        e.preventDefault();
        if ((username === "") & (password === "")) {
            return;
        } else {
            axios
                .post("http://localhost:8000/painter/login", {
                    username: username,
                    password: password,
                })
                .then((res) => {
                    if (res.data.status === "ok") {
                        localStorage.setItem('TokenPainter', res.data.token)
                        // console.log(res.data);
                        navigate("/Order");
                    }
                    else {
                        alert("Login failed!!")
                    }
                })
                .catch(function (error) {
                    console.log(error, "error");
                });
        }
    };

    return (
        <div>
            <Header />
            <section className="h-screen">
                <div className="px-6 h-full text-gray-800">
                    <div
                        className="flex xl:justify-center lg:justify-between justify-center items-center flex-wrap h-full g-6"
                    >
                        <div
                            className="grow-0 shrink-1 md:shrink-0 basis-auto xl:w-6/12 lg:w-6/12 md:w-9/12 mb-12 md:mb-0"
                        >
                            <img
                                src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
                                className="w-full"
                                alt=""
                            />
                        </div>
                        {getToken ? (
                            <Navigate to='/Order' state ={{from : location}}/>
                        ) : (
                            <div className="xl:ml-20 xl:w-5/12 lg:w-5/12 md:w-8/12 mb-12 md:mb-0">
                                <form>
                                    <div
                                        className="flex items-center my-4 before:flex-1 before:border-t before:border-gray-300 before:mt-0.5 after:flex-1 after:border-t after:border-gray-300 after:mt-0.5"
                                    >
                                        <p className="text-center text-lg font-semibold mx-4 mb-0">Painter</p>
                                    </div>

                                    {/* <!-- Username input --> */}
                                    <div className="mb-6">
                                        <input
                                            type="text"
                                            className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                            placeholder="Username"
                                            onChange={(e) => setUsername(e.target.value)}
                                        />
                                    </div>

                                    {/* <!-- Password input --> */}
                                    <div className="mb-6">
                                        <input
                                            type="password"
                                            className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                            placeholder="Password"
                                            onChange={(e) => setPassword(e.target.value)}
                                        />
                                    </div>

                                    {/* <div className="flex justify-between items-center mb-6">
                                        <div className="form-group form-check">
                                            <input
                                                type="checkbox"
                                                className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                                                id="exampleCheck2"
                                            />
                                            <label className="form-check-label inline-block text-gray-800" htmlFor="exampleCheck2"
                                            >Remember me</label
                                            >
                                        </div>
                                        <a href="#!" className="text-gray-800">Forgot password?</a>
                                    </div> */}

                                    <div className="flex justify-center text-center lg:text-left">
                                        <button
                                            type="button"
                                            className="inline-block px-7 py-3 bg-blue-600 text-white font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                                            onClick={login}
                                        >
                                            Login
                                        </button>
                                    </div>
                                    <div className='flex space-x-2 items-center font-semibold mt-2 pt-1 mb-10'>
                                        <p className="text-sm ">
                                            Don't have an account?
                                        </p>
                                        <a
                                            href="/RegisterPainter"
                                            className="text-red-600  hover:text-red-700 focus:text-red-700 transition duration-200 ease-in-out"
                                        >Register</a
                                        >
                                    </div>
                                </form>
                            </div>
                        )}
                    </div>
                </div>
            </section>
        </div>
    )
}

export default LoginPainter