import { React, useState, useEffect } from 'react'
import axios from "axios";
import Loading from "react-loading";
import { useNavigate } from "react-router-dom";
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// import Header from "../../component/Header"
import HeaderPainter from "../../component/HeaderPainter"

import NavbarPainter from './NavbarPainter';

import PopupAddimage from './component/PopupAddimage';
import PopupDeleteimage from './component/PopupDeleteimage';

function PerformancePainter() {
    const navigate = useNavigate();
    const [dataPainter, setDataPainter] = useState('');
    const [performanceImage, setPerformanceImage] = useState([])
    const [image_id, setImage_id] = useState("")
    const [popupAdd, setPopupAdd] = useState(true);
    const [popupDelete, setPopupDelete] = useState(true);
    const [loading, setLoading] = useState(true);
    const token = localStorage.getItem('TokenPainter')
    // console.log("image:", performanceImage)

    //---------------- data, packages painter ---------------//
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/MydataPainter/${token}`)
                .then((resp) => {
                    setPerformanceImage(resp.data.images);
                    setDataPainter(resp.data.data[0]);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [token]);

    //---------------- Nevigate params profile user ---------------//
    const painterClickProfile = (data) => {
        navigate(`/Profile/${data}`);
        // console.log("check:", data)
    };

    ///------------------ Popup Add image ---------------------///
    const popupAddImage = () => {
        setPopupAdd(!popupAdd)
    }

    ///------------------ Popup Delete image ---------------------///
    const popupDeleteImage = (id) => {
        setImage_id(id)
        setPopupDelete(!popupDelete)
    }

    return (

        <div className='bg-gray-200 w-full h-screen'>
            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className='bg-gray-200'>

                    {!popupAdd ?
                        <PopupAddimage popupAdd={popupAdd} setPopupAdd={setPopupAdd} ID_Painter={dataPainter.painter_id} />
                        : null
                    }

                    {!popupDelete ?
                        <PopupDeleteimage popupDelete={popupDelete} setPopupDelete={setPopupDelete} image_id={image_id} />
                        : null
                    }

                    <HeaderPainter painterClickProfile={painterClickProfile} />
                    {/* <!-- component --> */}
                    <div className='px-5 py-5 mx-auto container'>

                        <NavbarPainter />

                        <div className='mt-5'>

                            <div className='flex justify-between py-7 mx-14'>
                                <p className='font-bold text-[#615F5F] text-2xl'>ผลงานการวาดภาพ</p>
                                <button onClick={popupAddImage} className='bg-lime-600 rounded-md w-[10%] font-medium text-[16px] text-white hover:bg-lime-500'>เพิ่มรูปผลงาน</button>
                            </div>

                            <div className='container px-20 mx-auto'>
                                <div className="grid grid-cols-4 gap-5 ">
                                    {performanceImage.length === 0 ? (
                                        <div className="flex h-[300px] justify-center place-items-center space-x-2 font-skvb">
                                            <FontAwesomeIcon
                                                icon={faTriangleExclamation}
                                                className="text-yellow-300 mt-1 w-7 h-7"
                                            />
                                            <h1 className="mt-2">No Data Found</h1>
                                        </div>
                                    ) : (
                                        performanceImage.map((img, key) => {
                                            return (
                                                <div key={key} className="w-full">
                                                    <div onClick={() => popupDeleteImage(img.image_id)} className="mdi mdi-close absolute ml-[230px] mt-2 hover:text-red-500 cursor-pointer">X</div>
                                                    <img className="h-[310px] w-full rounded-md" src={img.image_base64} alt="" />
                                                </div>
                                            )
                                        })
                                    )}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}

export default PerformancePainter