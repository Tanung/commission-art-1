import axios from "axios";
import { React } from "react";

function PopupStatus({ popupStatus, setPopupStatus, id_order, dataStatus }) {

    ///------------------ Popup change status ---------------------///
    const clickStatus = async (e) => {
        e.preventDefault()
        await axios
            .put(`http://localhost:8000/painter/click/status/${id_order}`, {
                order_status: dataStatus,
            })
            .then((resp) => {
                // console.log("Resp:", resp.data);
                setPopupStatus(!popupStatus)
                window.location.reload();
            })
            .catch((err) => console.log(err));
    }

    return (
        <div className="justify-center items-center text-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            <div className="bg-white grid grid-rows-3 rounded py-4 h-auto w-[23%] text-black">
                <div className="row-start-1 grid items-center">
                    <div className="text-2xl font-medium text-red-600">
                        Are you sure?
                    </div>

                </div>
                <div className="row-start-2 mt-5 space-y-4">
                    <div>Do you want to change status?</div>

                </div>
                <div className='row-start-3 mt-5 flex items-end justify-center space-x-5'>
                    <div className="grid font-skvsb">
                        <button
                            type="submit"
                            className="bg-red-600 text-white rounded w-24 h-8"
                            onClick={clickStatus}
                        >
                            Yes
                        </button>
                    </div>
                    <div className="grid font-skvsb">
                        <button
                            type="submit"
                            className="bg-gray-400 text-white rounded w-24 h-8"
                            onClick={() => setPopupStatus(!popupStatus)}
                        >
                            No
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PopupStatus