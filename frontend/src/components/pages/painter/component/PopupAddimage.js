import { React, useState } from "react";
import axios from 'axios'

function PopupAddimage({ popupAdd, setPopupAdd, ID_Painter }) {

    const [files, setFile] = useState("");
    const [message, setMessage] = useState();
    console.log("image:", files)
    // console.log("id:", ID_Painter)

    //------------------ Choose image ---------------------///
    const Imagebase64 = (e) => {
        e.preventDefault()
        setMessage("");
        const file = e.target.files[0];
        const reader = new FileReader();

        reader.onloadend = () => {
            setFile(reader.result.toString());
        };
        reader.readAsDataURL(file)
    };
    const removeImage = () => {
        setFile('');
    }

    //----------------------------------- Submit --------------------------------------///
    const SubmitAddImage = async (e) => {
        e.preventDefault()

        await axios
            .post(`http://localhost:8000/painter/add/image`, {
                painter_id: ID_Painter,
                image_base64: files,
            })
            .then((resp) => {
                // console.log("Resp:", resp);
                setPopupAdd(!popupAdd)
                window.location.reload();
            })
            .catch((err) => console.log(err));
    }

    return (
        <div className="justify-center items-center py-5 text-center flex overflow-x-hidden overflow-y-scroll fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            <div className="bg-white px-8 py-5 rounded h-max w-[40%] text-black">
                <form onSubmit={SubmitAddImage}>
                    <div className="grid grid-cols-3 text-2xl font-medium text-[#3F91DD]">
                        <div className="col-start-2 text-center">เพิ่มรูปภาพ</div>
                        <button onClick={() => setPopupAdd(!popupAdd)} className="col-start-3 font-semibold text-3xl text-red-500 text-right">x</button>
                    </div>

                    <div className="flex justify-center mt-5 items-center px-3">
                        <div className="rounded-lg shadow-xl bg-gray-50 w-[80%]">
                            <div className="m-4">
                                <span className="flex justify-center items-center text-[12px] mb-1 text-red-500">{message}</span>
                                <div className="flex items-center justify-center w-full">
                                    <label className="flex cursor-pointer flex-col w-full h-32 border-2 rounded-md border-dashed hover:bg-gray-100 hover:border-gray-300">
                                        <div className="flex flex-col items-center justify-center pt-7">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                className="w-12 h-12 text-gray-400 group-hover:text-gray-600" viewBox="0 0 20 20"
                                                fill="currentColor">
                                                <path filerule="evenodd"
                                                    d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z"
                                                    clipRule="evenodd" />
                                            </svg>
                                            <p className="pt-1 text-sm tracking-wider text-gray-400 group-hover:text-gray-600">
                                                Select a photo</p>
                                        </div>
                                        <input type="file" onChange={(e) => Imagebase64(e)} className="opacity-0" multiple="multiple" name="files[]" />
                                    </label>
                                </div>
                                <div className="flex flex-wrap gap-2 mt-2">

                                    {files.length !== 0 ?
                                        (<div className="overflow-hidden relative mt-2">
                                            <div onClick={removeImage} className="mdi mdi-close absolute ml-[67px] hover:text-red-500 cursor-pointer">x</div>
                                            <img className="h-20 w-20 rounded-md" src={files} alt="" />
                                        </div>) :
                                        null
                                    }



                                </div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" className="mt-8 h-9 w-24 text-white rounded bg-lime-400 hover:bg-lime-500">
                        เพิ่ม
                    </button>

                </form>
            </div>
        </div>
    )
}

export default PopupAddimage