import { React, useState } from "react";
import axios from 'axios'

function PopupSubmitWork({ popupSubmitWork, setPopupSubmitWork, id_order }) {

    const [files, setFile] = useState("");
    // console.log("image:", files)
    // console.log("id:", id_order)

    //------------------ Choose image ---------------------///
    const Imagebase64 = async (e) => {
        e.preventDefault()
        const file = e.target.files[0];
        const reader = new FileReader();

        reader.onloadend = () => {
            setFile(reader.result.toString());
        };
        await reader.readAsDataURL(file)
    };
    const removeImage = () => {
        setFile('');
    }

    //----------------------------------- Submit --------------------------------------///
    const SubmitWork = async (e) => {
        e.preventDefault()
        await axios
            .put(`http://localhost:8000/painter/submitwork/image/${id_order}`, {
                order_status: 8,
                product_img: files,
            })
            .then((resp) => {
                // console.log("Resp:", resp.data);
                setPopupSubmitWork(!popupSubmitWork)
                window.location.reload();
            })
            .catch((err) => console.log(err));
    }

    return (
        <div className="justify-center items-center py-5 text-center flex overflow-x-hidden overflow-y-scroll fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            <div className="bg-white px-8 py-5 rounded h-max w-[40%] text-black">
                <form onSubmit={SubmitWork}>
                    <div className="grid grid-cols-3 text-2xl font-medium text-[#3F91DD]">
                        <div className="col-start-2 text-center">ส่งงาน</div>
                        <button onClick={() => setPopupSubmitWork(!popupSubmitWork)} className="col-start-3 font-semibold text-3xl text-red-500 text-right">x</button>
                    </div>

                    <div className="flex justify-center mt-5 items-center px-3">
                        <div className="rounded-lg shadow-xl bg-gray-50 w-[80%]">
                            <div className="m-4">
                                <div className="flex items-center justify-center w-full">
                                    <label className="flex cursor-pointer flex-col w-full h-32 border-2 rounded-md border-dashed hover:bg-gray-100 hover:border-gray-300">
                                        <div className="flex flex-col items-center justify-center pt-7">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                className="w-12 h-12 text-gray-400 group-hover:text-gray-600" viewBox="0 0 20 20"
                                                fill="currentColor">
                                                <path filerule="evenodd"
                                                    d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z"
                                                    clipRule="evenodd" />
                                            </svg>
                                            <p className="pt-1 text-sm tracking-wider text-gray-400 group-hover:text-gray-600">
                                                Select a photo</p>
                                        </div>
                                        <input type="file" onChange={(e) => Imagebase64(e)} className="opacity-0" multiple="multiple" name="files[]" />
                                    </label>
                                </div>
                                <div className="flex flex-wrap gap-2 mt-2">

                                    {files.length !== 0 ?
                                        (<div className="overflow-hidden relative mt-2">
                                            <div onClick={removeImage} className="mdi mdi-close absolute ml-[67px] hover:text-red-500 cursor-pointer">x</div>
                                            <img className="h-20 w-20 rounded-md" src={files} alt="" />
                                        </div>) :
                                        null
                                    }



                                </div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" className="mt-8 h-9 w-24 text-white rounded bg-lime-400 hover:bg-lime-500">
                        ส่ง
                    </button>

                </form>
            </div>
        </div>
    )
}

export default PopupSubmitWork