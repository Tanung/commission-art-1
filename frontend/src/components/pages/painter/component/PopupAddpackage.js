import React, { useState } from 'react'
import axios from 'axios'

function PopupAddpackage({ popupPackage, setPopupPackage, ID_Painter }) {
    // const navigate = useNavigate();
    const [dataPackage, setdataPackage] = useState({
        package_name: '',
        package_character: '',
        package_bgcolor: '',
        package_size: '',
        package_resolution: '',
        package_file: '',
        package_price: ''
    });
    // console.log("dataPackage:", dataPackage)
    // console.log("ID Painter:", ID_Painter)

    //----------------------------------- Add package --------------------------------------///
    const onChangePG = (e) => {
        e.preventDefault();
        const newPainter = { ...dataPackage };
        newPainter[e.target.name] = e.target.value;
        setdataPackage(newPainter)
    }

    //----------------------------------- Submit --------------------------------------///
    const clickAddPackage = async (e) => {
        e.preventDefault()
        // console.log("คลิ๊กๆๆๆ")
        await axios
            .post(`http://localhost:8000/painter/add/package`, {
                painter_id: ID_Painter,
                package_name: dataPackage.package_name,
                package_character: dataPackage.package_character,
                package_bgcolor: dataPackage.package_bgcolor,
                package_size: dataPackage.package_size,
                package_resolution: dataPackage.package_resolution,
                package_file: dataPackage.package_file,
                package_price: dataPackage.package_price
            })
            .then((resp) => {
                // console.log("Resp:", resp.data);
                setPopupPackage(!popupPackage)
                window.location.reload();
            })
            .catch((err) => console.log(err));
    }

    return (
        <div className="justify-center py-8 items-center text-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            <div className="bg-white rounded-lg h-full w-[50%] text-black">
                <div className="px-6 py-6 h-full">
                    <form onSubmit={clickAddPackage}>
                        <h1 className="mb-8 text-2xl text-center">รายละเอียดแพ็กเกจ</h1>
                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="package_name"
                            value={dataPackage.package_name}
                            onChange={onChangePG}
                            placeholder="ชื่อแพ็กเกจ" />

                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="package_character"
                            value={dataPackage.package_character}
                            onChange={onChangePG}
                            placeholder="ตัวละคร" />

                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="package_bgcolor"
                            value={dataPackage.package_bgcolor}
                            onChange={onChangePG}
                            placeholder="สีพื้นหลัง" />

                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="package_size"
                            value={dataPackage.package_size}
                            onChange={onChangePG}
                            placeholder="ขนาดรูปภาพ" />

                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="package_resolution"
                            value={dataPackage.package_resolution}
                            onChange={onChangePG}
                            placeholder="ความละเอียดภาพ" />

                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="package_file"
                            value={dataPackage.package_file}
                            onChange={onChangePG}
                            placeholder="ไฟล์รูปภาพ" />

                        <input
                            type="text"
                            className="block border border-grey-light w-full p-3 rounded mb-4"
                            name="package_price"
                            value={dataPackage.package_price}
                            onChange={onChangePG}
                            placeholder="ราคา" />

                        <div className='flex items-end justify-center space-x-5'>
                            <div className="grid font-skvsb">
                                <button
                                    type="submit"
                                    className="bg-[#5FCB39] text-white rounded w-24 h-8"
                                >
                                    Add
                                </button>
                            </div>
                            <div className="grid font-skvsb">
                                <button 
                                    className="bg-gray-400 text-white rounded w-24 h-8"
                                    onClick={() => setPopupPackage(!popupPackage)}
                                >
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    )
}

export default PopupAddpackage