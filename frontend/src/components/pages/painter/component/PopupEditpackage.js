import axios from "axios";
import Loading from "react-loading";
import { React, useEffect, useState } from "react";

// import HeaderPainter from "../../component/HeaderPainter"

function PopupEditpackage({ popupEdit, setPopupEdit, id_package }) {
    const [dataPackage, setdataPackage] = useState('');
    const [loading, setLoading] = useState(true);

    //---------------- get api user ---------------//
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/painter/package/${id_package}`)
                .then((resp) => {
                    setdataPackage(resp.data[0]);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [id_package]);

    //---------------- OnChange ---------------//
    const onChangeEdit = (e) => {
        e.preventDefault();
        const newData = { ...dataPackage };
        newData[e.target.name] = e.target.value;
        setdataPackage(newData)
    }

    //---------------- onSubmit ---------------//
    const clickEditPackage = (e) => {
        e.preventDefault();
        axios.put(`http://localhost:8000/painter/edit/package/${id_package}`, {
            package_name: dataPackage.package_name,
            package_character: dataPackage.package_character,
            package_bgcolor: dataPackage.package_bgcolor,
            package_size: dataPackage.package_size,
            package_resolution: dataPackage.package_resolution,
            package_file: dataPackage.package_file,
            package_price: dataPackage.package_price
        })

            .then((res) => {
                // console.log(res.data);
                setPopupEdit(!popupEdit)
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });
    }

    return (

        <div className="justify-center py-8 items-center text-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className="bg-white rounded-lg h-full w-[50%] text-black">
                    <div className="px-6 py-6 h-full">
                        <form onSubmit={clickEditPackage}>
                            <h1 className="mb-8 text-2xl text-center">แก้ไขแพ็กเกจ</h1>
                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="package_name"
                                value={dataPackage.package_name}
                                onChange={onChangeEdit}
                                placeholder="ชื่อแพ็กเกจ" />

                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="package_character"
                                value={dataPackage.package_character}
                                onChange={onChangeEdit}
                                placeholder="ตัวละคร" />

                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="package_bgcolor"
                                value={dataPackage.package_bgcolor}
                                onChange={onChangeEdit}
                                placeholder="สีพื้นหลัง" />

                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="package_size"
                                value={dataPackage.package_size}
                                onChange={onChangeEdit}
                                placeholder="ขนาดรูปภาพ" />

                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="package_resolution"
                                value={dataPackage.package_resolution}
                                onChange={onChangeEdit}
                                placeholder="ความละเอียดภาพ" />

                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="package_file"
                                value={dataPackage.package_file}
                                onChange={onChangeEdit}
                                placeholder="ไฟล์รูปภาพ" />

                            <input
                                type="text"
                                className="block border border-grey-light w-full p-3 rounded mb-4"
                                name="package_price"
                                value={dataPackage.package_price}
                                onChange={onChangeEdit}
                                placeholder="ราคา" />

                            <div className='flex items-end justify-center space-x-5'>
                                <div className="grid font-skvsb">
                                    <button
                                        type="submit"
                                        className="bg-[#5FCB39] text-white rounded w-24 h-8"
                                    >
                                        Edit
                                    </button>
                                </div>
                                <div className="grid font-skvsb">
                                    <button
                                        className="bg-gray-400 text-white rounded w-24 h-8"
                                        onClick={() => setPopupEdit(!popupEdit)}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            )}
        </div>

    )
}

export default PopupEditpackage