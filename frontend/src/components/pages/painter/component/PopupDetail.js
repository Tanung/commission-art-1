import { React, useEffect, useState } from "react";
import axios from "axios";
import Loading from "react-loading";

function PopupDetail({ popupDetail, setPopupDetail, id_order }) {

    const [dataOrder, setDataOrder] = useState('');
    const [loading, setLoading] = useState(true);
    // console.log("dataOrder:", dataOrder)
    // console.log("dataPainter:", dataPainter)

    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/painter/see/order_deteil/${id_order}`)
                .then((resp) => {
                    // console.log("resp", resp.data)
                    setDataOrder(resp.data.order);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [id_order]);

    return (
        <div className="justify-center py-5 text-center flex overflow-x-hidden overflow-y-scroll fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className="bg-white px-8 py-5 rounded h-max w-[70%] text-black">
                    <div className="grid grid-cols-3 text-2xl font-medium text-[#3F91DD]">
                        <div className="col-start-2 text-center">รายละเอียดออเดอร์</div>
                        <button onClick={() => setPopupDetail(!popupDetail)} className="col-start-3 font-semibold text-3xl text-red-500 text-right">x</button>
                    </div>

                    <div className="grid grid-cols-3 mt-7 text-left">
                        <div className="">ชื่อผู้จอง: {dataOrder.user_name}</div>
                        <div className="">เบอร์โทร: {dataOrder.user_phone}</div>
                        <div className="">อีเมล์: {dataOrder.user_email}</div>
                    </div>

                    <div className="grid grid-rows-8 mt-5 text-left">
                        <div className="row-start-1 grid grid-cols-7">
                            <div className="">แพ็กเกจที่เลือก: </div>
                            <div className="col-start-2 col-span-5">แพ็กเกจ: {dataOrder.package_name}</div>
                        </div>
                        <div className="row-start-2 grid grid-cols-7">
                            <div className="col-start-2 col-span-5">ตัวละคร: {dataOrder.package_character}</div>
                        </div>
                        <div className="row-start-3 grid grid-cols-7">
                            <div className="col-start-2 col-span-5">สีตัวละคร: #######วาดภาพแนว miminal######</div>
                        </div>
                        <div className="row-start-4 grid grid-cols-7">
                            <div className="col-start-2 col-span-5">สีพื้นหลัง: {dataOrder.package_bgcolor}</div>
                        </div>
                        <div className="row-start-5 grid grid-cols-7">
                            <div className="col-start-2 col-span-5">ความละเอียดภาพ: {dataOrder.package_resolution}</div>
                        </div>
                        <div className="row-start-6 grid grid-cols-7">
                            <div className="col-start-2 col-span-5">ขนาดรูปภาพ: {dataOrder.package_size}</div>
                        </div>
                        <div className="row-start-7 grid grid-cols-7">
                            <div className="col-start-2 col-span-5">ส่งงานเป็นไฟล์: {dataOrder.package_file}</div>
                        </div>
                        <div className="row-start-8 grid grid-cols-7">
                            <div className="col-start-2 col-span-5">ราคา: {dataOrder.package_price} บาท</div>
                        </div>
                    </div>

                    <div className="text-left mt-5">
                        เลือกรูปภาพที่ต้องการให้วาด:
                    </div>

                    <div className="flex justify-center mt-5">
                        <div className="h-3/5 w-3/5">
                            <img className=" h-full w-full rounded-md" src={dataOrder.order_image} alt="" />
                        </div>
                    </div>
                </div>
            )}
        </div>
    )


}

export default PopupDetail