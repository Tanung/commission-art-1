import axios from "axios";
import { React, useState } from "react";

function PopupApprove({ popupApprove, setPopupApprove, id_order }) {
    const [dateTime, setDateTime] = useState({
        order_dateQ: "",
        order_deadline: "",
    })
    // console.log("555:", dateTime)

    ///------------------ Popup cancel order ---------------------///
    const clickApprove = async (e) => {
        e.preventDefault()
        if (dateTime.order_dateQ === '' && dateTime.order_deadline === '') {
            e.preventDefault();
            alert("กรุณาใส่วันที่ได้คิวเเละวันส่งงาน");
        } else if (dateTime.order_dateQ !== '' && dateTime.order_deadline === '') {
            e.preventDefault();
            alert("กรุณาใส่วันส่งงาน");
        } else if (dateTime.order_dateQ === '' && dateTime.order_deadline !== '') {
            e.preventDefault();
            alert("กรุณาใส่วันที่ได้คิว");
        } else {
            e.preventDefault();
            await axios.put(`http://localhost:8000/painter/click/approve/${id_order}`, {
                order_status: 2,
                order_dateQ: dateTime.order_dateQ,
                order_deadline: dateTime.order_deadline,
            })
                .then((res) => {
                    console.log("Response:", res.data);
                    setPopupApprove(!popupApprove)
                    window.location.reload();
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }

    //----------------------------------- รับข้อมูลจาก input --------------------------------------///
    const onChangeDate = (e) => {
        e.preventDefault();
        const date = { ...dateTime };
        date[e.target.name] = e.target.value;
        setDateTime(date)
    }

    return (
        <div className="justify-center items-center text-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none w-full h-full bg-[#C4C4C4] bg-opacity-70">
            <div className="bg-white grid rounded py-4 h-auto w-[23%] text-black">
                <div className="row-start-1 grid items-center">
                    <div className="text-2xl font-medium text-red-600">
                        กำหนดวัน 
                    </div>

                </div>
                <div className="mt-4 space-y-4">
                    <div className="flex items-center justify-center space-x-2">
                        <h1>วันที่ได้คิว</h1>
                        <div className="relative">
                            <input onChange={onChangeDate} value={dateTime.order_dateQ} name="order_dateQ" type="date" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Select date start" />
                        </div>
                    </div>
                    <div className="flex items-center justify-center space-x-2">
                        <h1>วันส่งงาน</h1>
                        <div className="relative">
                            <input onChange={onChangeDate} value={dateTime.order_deadline} name="order_deadline" type="date" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Select date end" />
                        </div>

                    </div>

                </div>
                <div className='flex items-end mt-4 justify-center space-x-5'>
                    <div className="grid font-skvsb">
                        <button
                            type="submit"
                            className="bg-lime-600 text-white rounded w-24 h-8"
                            onClick={clickApprove}
                        >
                            Yes
                        </button>
                    </div>
                    <div className="grid font-skvsb">
                        <button
                            type="submit"
                            className="bg-gray-400 text-white rounded w-24 h-8"
                            onClick={() => setPopupApprove(!popupApprove)}
                        >
                            No
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PopupApprove