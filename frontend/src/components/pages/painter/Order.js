import { React, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Loading from "react-loading";
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// import Header from "../../component/Header"
import HeaderPainter from "../../component/HeaderPainter"
import NavbarPainter from '../painter/NavbarPainter';
import PopupApprove from "./component/PopupApprove";
import PopupDisapprove from "./component/PopupDisapprove";
import PopupAddqueue from "./component/PopupAddqueue";
import PopupDetail from "./component/PopupDetail"

function Order() {
    const navigate = useNavigate();
    const [dataOrder, setDataOrder] = useState([])
    const [dataAllStatus, setAllStatus] = useState('');
    const [popupApprove, setPopupApprove] = useState(true)
    const [popupDisapprove, setPopupDisapprove] = useState(true)
    const [popupAddQueue, setPopupAddQueue] = useState(true)
    const [popupDetail, setPopupDetail] = useState(true)
    const [id_order, setId_order] = useState([]);
    const [loading, setLoading] = useState(true);
    const token = localStorage.getItem('TokenPainter')
    // console.log("555: ", dataAllStatus)


    //----------------------------------- get order user --------------------------------------///
    useEffect(() => {
        (async () => {
            await axios
                .get(`http://localhost:8000/MydataPainter/${token}`)
                .then((resp) => {
                    // console.log("resp:", resp.data.users)
                    setDataOrder(resp.data.orders);
                    setAllStatus(resp.data.status)
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        })();
    }, [token]);

    //---------------- Nevigate params profile user ---------------//
    const painterClickProfile = (data) => {
        navigate(`/Profile/${data}`);
    };

    ///------------------ Popup Approve ---------------------///
    const popup_Approve = (id) => {
        setId_order(id)
        setPopupApprove(!popupApprove)
    }

    ///------------------ Popup Disapprove ---------------------///
    const popup_Disapprove = (id) => {
        setId_order(id)
        setPopupDisapprove(!popupDisapprove)
    }

    ///------------------ Popup Add queue ---------------------///
    const popup_Add_Queue = (id) => {
        setId_order(id)
        setPopupAddQueue(!popupAddQueue)
    }

    ///------------------ Popup Detail ---------------------///
    const popup_Detail = (id) => {
        setId_order(id)
        setPopupDetail(!popupDetail)
    }

    return (

        <div className='bg-gray-200 w-full h-screen'>
            {loading === true ? (
                <div className="grid">
                    <div className="grid justify-items-center mt-80">
                        <Loading
                            type="bubbles"
                            color="#0000FF"
                            height={200}
                            width={100}
                        />
                    </div>
                </div>
            ) : (
                <div className='bg-gray-200'>
                    <HeaderPainter painterClickProfile={painterClickProfile} />

                    {!popupApprove ?
                        <PopupApprove popupApprove={popupApprove} setPopupApprove={setPopupApprove} id_order={id_order} />
                        : null
                    }

                    {!popupDisapprove ?
                        <PopupDisapprove popupDisapprove={popupDisapprove} setPopupDisapprove={setPopupDisapprove} id_order={id_order} />
                        : null
                    }
                    
                    {!popupAddQueue ?
                        <PopupAddqueue popupAddQueue={popupAddQueue} setPopupAddQueue={setPopupAddQueue} id_order={id_order} />
                        : null
                    }

                    {!popupDetail ?
                        <PopupDetail popupDetail={popupDetail} setPopupDetail={setPopupDetail} id_order={id_order} />
                        : null
                    }

                    <div className='px-5 py-5 mx-auto container'>

                        <NavbarPainter />

                        <div className='mt-5'>

                            <div className='font-bold text-[#615F5F] py-7 text-2xl ml-14'>
                                ข้อมูลการจอง
                            </div>

                            <table className="min-w-full text-center border-collapse block md:table">
                                <thead className="block md:table-header-group">
                                    <tr className="border border-grey-500 md:border-none block md:table-row absolute -top-full md:top-auto -left-full md:left-auto  md:relative ">
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ลำดับ</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ชื่อผู้จอง</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">เบอร์โทร</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">ราคา</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">วันที่ได้คิว</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">วันส่งงาน</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">สถานะ</th>
                                        <th className="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 block md:table-cell">Actions</th>
                                    </tr>
                                </thead>

                                {dataOrder.length === 0 ? (
                                    <tbody className="flex h-full justify-center mt-32 space-x-2 font-skvb">
                                        <tr className=''>
                                            <td>
                                                <FontAwesomeIcon
                                                    icon={faTriangleExclamation}
                                                    className="text-yellow-300 mt-1 w-7 h-7"
                                                />
                                                <h1 className="mt-2">No Data Found</h1>
                                            </td>
                                        </tr>
                                    </tbody>
                                ) : dataOrder.map((data, key) => {
                                    return (
                                        <tbody key={key} className="bg-white block md:table-row-group">
                                            <tr className="border border-grey-500 md:border-none block md:table-row">
                                                <td className="w-10 p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold"></span>{key + 1}</td>
                                                <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">ชื่อผู้จอง</span>{data.user_name}</td>
                                                <td className="w-24 p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">เบอร์โทร</span>{data.user_phone}</td>
                                                <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">ราคา</span>{data.package_price}</td>
                                                <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">วันที่ได้คิว</span>{data.order_dateQ}</td>
                                                <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">วันส่งงาน</span>{data.order_deadline}</td>
                                                <td className="p-2 md:border md:border-grey-500 block md:table-cell"><span className="inline-block w-1/3 md:hidden font-bold">สถานะ</span>
                                                    {dataAllStatus.map((dataStatus, key) => {
                                                        return (
                                                            <div key={key}>
                                                                {dataStatus.status_id === data.order_status ? dataStatus.status : null}
                                                            </div>
                                                        )
                                                    })}
                                                </td>
                                                <td className="w-[20%] p-2 md:border md:border-grey-500 block md:table-cell">
                                                    <span className="inline-block w-1/3 md:hidden font-bold">Actions</span>
                                                    <div className='flex space-x-1 items-center justify-center'>

                                                        {/* ถ้า status = 2 ให้ซ่อนปุ่ม รับงานกับไม่รับงาน ถ้าไม่ใช่ให้เเสดงเห็นเดิม */}
                                                        {data.order_status === 1 ?
                                                            <>
                                                                <button onClick={() => popup_Approve(data.order_id)} type="submit" className="bg-lime-500 hover:bg-lime-600 text-white font-bold py-1 px-2 border border-lime-400 rounded">รับงาน</button>
                                                                <button onClick={() => popup_Disapprove(data.order_id)} className="bg-red-600 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-500 rounded">ไม่รับงาน</button>
                                                            </>
                                                            : <></>}

                                                        {data.order_status === 4 ?
                                                            <button onClick={() => popup_Add_Queue(data.order_id)} className="bg-lime-500 hover:bg-lime-600 text-white font-bold py-1 px-2 border border-lime-400 rounded">เพิ่มคิว</button>
                                                            : <></>}

                                                        <button onClick={() => popup_Detail(data.order_id)} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 border border-blue-500 rounded">รายละเอียด</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    )
                                })}

                            </table>
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}

export default Order