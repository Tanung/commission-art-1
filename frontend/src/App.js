import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import { RequireToken } from "./components/Authentication/Auth";
import { RequireTokenAdmin } from "./components/Authentication/Auth_admin";
import { RequireTokenPainter } from "./components/Authentication/Auth_painter";

//--------------- หน้า user -----------------//
import Home from "./components/pages/user/Home"
// import Test from './components/pages/user/Test';
import LoginUser from "./components/pages/user/LoginUser"
import RegisterUser from "./components/pages/user/RegisterUser"
import Detail from "./components/pages/user/Detail"
import Mytable from "./components/pages/user/Mytable"
import Profile from "./components/pages/user/Profile"

//--------------- หน้า Painter -----------------//
import LoginPainter from "./components/pages/painter/LoginPainter"
import RegisterPainter from "./components/pages/painter/RegisterPainter"
import ProfilePainter from "./components/pages/painter/ProfilePainter"
import Order from "./components/pages/painter/Order"
import QueuePainter from "./components/pages/painter/QueuePainter"
import PackagePainter from "./components/pages/painter/PackagePainter"
import PerformancePainter from "./components/pages/painter/PerformancePainter"
import TestImage555 from "./components/pages/painter/TestImage555"

//--------------- หน้า Admin -----------------//
import LoginAdmin from "./components/pages/admin/LoginAdmin"
import Dashboard from "./components/pages/admin/Dashboard"
import PainterPage from "./components/pages/admin/PainterPage"

function App() {
  return (
    <div className="">
      <BrowserRouter>
        {/* <RequireToken> */}

        <Routes>
          {/* -------------------- Route user ------------------- */}
          <Route path="/" element={<LoginUser />} />
          <Route path="/RegisterUser" element={<RegisterUser />} />
          <Route path="/Home" element={<RequireToken><Home /></RequireToken>} />
          <Route path="/Home/Mytable" element={<RequireToken><Mytable /></RequireToken>} />
          <Route path="/Home/Detail/:id_painter" element={<RequireToken><Detail /></RequireToken>} />
          <Route path="/Home/Profile/:id_user" element={<RequireToken><Profile /></RequireToken>} />

          {/* -------------------- Route painter ------------------- */}
          <Route path="/LoginPainter" element={<LoginPainter />} />
          <Route path="/RegisterPainter" element={<RegisterPainter />} />
          <Route path="/Order" element={<RequireTokenPainter><Order /></RequireTokenPainter>} />
          <Route path="/QueuePainter" element={<RequireTokenPainter><QueuePainter /></RequireTokenPainter>} />
          <Route path="/PackagePainter" element={<RequireTokenPainter><PackagePainter /></RequireTokenPainter>} />
          <Route path="/PerformancePainter" element={<RequireTokenPainter><PerformancePainter /></RequireTokenPainter>} />
          <Route path="/Profile/:id_painter" element={<RequireTokenPainter><ProfilePainter /></RequireTokenPainter>} />
          <Route path="/TestImage555" element={<TestImage555 />} />


          {/* -------------------- Route admin ------------------- */}
          <Route path="/LoginAdmin" element={<LoginAdmin />} />
          <Route path="/Dashboard" element={<RequireTokenAdmin><Dashboard /></RequireTokenAdmin>} />
          <Route path="/PainterPage" element={<RequireTokenAdmin><PainterPage /></RequireTokenAdmin>} />

        </Routes>
        {/* </RequireToken> */}

      </BrowserRouter>
    </div>
  );
}

export default App;
