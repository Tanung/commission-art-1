from config.database import meta
from sqlalchemy import Table, Column, ForeignKey
from sqlalchemy.sql.sqltypes import Integer, String, Text, TIMESTAMP

#----------------- table users -----------------#
customers=Table(
    'customers', meta,
    Column('id', Integer, primary_key=True),
    Column('name', String(255)),
    Column('email', String(255)),
    Column('phone', String(11)),
    Column('username', String(255)),
    Column('password', String(255)),
    Column('image', Text),
    Column('role', String(50)),
    Column('date_create', TIMESTAMP),
)

#----------------- table painters -----------------#
painters=Table(
    'painters', meta,
    Column('painter_id', Integer, primary_key=True),
    Column('painter_name', String(255)),
    Column('painter_email', String(255)),
    Column('painter_phone', String(11)),
    Column('painter_style', String(50)),
    Column('painter_username', String(255)),
    Column('painter_password', String(255)),
    Column('painter_image', Text),
    Column('role', String(50)),
    Column('painter_date_create', TIMESTAMP),
)

#----------------- table users -----------------#
admin=Table(
    'admin', meta,
    Column('admin_id', Integer, primary_key=True),
    Column('admin_name', String(255)),
    Column('admin_email', String(255)),
    Column('admin_username', String(255)),
    Column('admin_password', String(255)),
    Column('role', String(50)),
)

#----------------- table package painter -----------------#
packages=Table(
    'packages', meta,
    Column('package_id', Integer, primary_key=True),
    Column('painter_id', Integer, ForeignKey('painters.painter.id')),
    Column('package_name', String(50)),
    Column('package_character', String(50)),
    Column('package_bgcolor', String(50)),
    Column('package_size', String(50)),
    Column('package_resolution', String(50)),
    Column('package_file', String(50)),
    Column('package_price', String(50)),
)

#----------------- table Images painter -----------------#
images=Table(
    'images', meta,
    Column('image_id', Integer, primary_key=True),
    Column('painter_id', Integer, ForeignKey('painters.painter.id')),
    Column('image_base64', Text),
)

#----------------- table orders -----------------#
orders=Table(
    'orders', meta,
    Column('order_id', Integer, primary_key=True),
    Column('user_id', Integer, ForeignKey('users.user.id')),
    Column('painter_id', Integer, ForeignKey('painters.painter.id')),
    Column('package_id', Integer, ForeignKey('packages.package.id')),
    Column('order_image', Text),
    Column('order_status', Integer),
    Column('order_detail', Text),
    Column('order_dateQ', String(50)),
    Column('order_deadline', String(50)),
    Column('order_create', TIMESTAMP),
    Column('user_name', String(100)),
    Column('user_phone', String(11)),
    Column('user_email', String(50)),
    Column('painter_name', String(100)),
    Column('painter_phone', String(11)),
    Column('painter_style', String(50)),
    Column('painter_email', String(50)),
    Column('package_name', String(100)),
    Column('package_character', String(50)),
    Column('package_bgcolor', String(100)),
    Column('package_size', String(50)),
    Column('package_resolution', String(50)),
    Column('package_file', String(50)),
    Column('package_price', String(50)),
)


#----------------- table ratings -----------------#
ratings=Table(
    'ratings', meta,
    Column('rating_id', Integer, primary_key=True),
    Column('user_id', Integer, ForeignKey('painters.painter.id')),
    Column('painter_id', Integer, ForeignKey('painters.painter.id')),
    Column('rating', String(10)),
    Column('rating_create', TIMESTAMP),
)

#----------------- table order_status -----------------#
order_status=Table(
    'order_status', meta,
    Column('status_id', Integer, primary_key=True),
    Column('status', String(100)),
)

#----------------- table products -----------------#
products=Table(
    'products', meta,
    Column('product_id', Integer, primary_key=True),
    Column('order_id', Integer, ForeignKey('orders.order.id')),
    Column('product_img', Text),
)