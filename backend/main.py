import jwt
from datetime import datetime
import uvicorn
from config.database import conn
from fastapi import (FastAPI)
from fastapi.encoders import jsonable_encoder
from fastapi.middleware.cors import CORSMiddleware
from models.users import admin, packages, painters, customers, images, orders, ratings, order_status, products
from schemas.user import (Admin_update, LoginAdmin, LoginItem, LoginPainter,
                          Painter, Customer, User_edit_profile, Painter_edit_profile, 
                          Painter_edit_package, Painter_add_package, 
                          Painter_Add_Image, Order_user, Order_approve, Order_disapprove, 
                          Order_confirm, User_give_rating, Painter_change_status,
                          Painter_add_queue, Painter_submit_work, Authentication, Painter_Authentication, Admin_Authentication)

db = conn

# secert of token
SECRET_KEY = "YOUR_FAST_API_SECRET_KEY"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRES_MINUTES = 800

origins = {
    "http://localhost:3000",
}

app = FastAPI()  # สร้าง app มาเพื่อเป็น instance ของ Class FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
###--------------------------------------- Data for everyone -----------------------------------------###

##-------------------- Data API users ----------------------##
@app.get("/api/users")
async def api_users():  # type: ignore
    data = db.execute(customers.select()).fetchall()
    return data

##-------------------- Header User ----------------------##
@app.get("/headerUser/{token}")
async def HeaderUser(token: str):
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    data = db.execute(customers.select().where(customers.c.id == payload["id"])).fetchall()
    return data  # type: ignore

##-------------------- Header painter ----------------------##
@app.get("/headerPainter/{token}")
async def HeaderPainter(token: str):
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    data = db.execute(painters.select().where(painters.c.painter_id == payload["id"])).fetchall()
    return data  # type: ignore

##-------------------- Header Admin ----------------------##
@app.get("/headerAdmin/{token}")
async def HeaderAdmin(token: str):
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    data = db.execute(admin.select().where(admin.c.admin_id == payload["id"])).fetchall()
    return data  # type: ignore


###--------------------------------------- User -----------------------------------------###

##-------------------- login user ----------------------##
@app.post("/user/login")
async def user_login(login: LoginItem):
    try:
        data = db.execute(customers.select().where(customers.c.username == login.username and customers.c.password == login.password)).first()
        if data.username == login.username and data.password == login.password: # type: ignore
            create_access_token = jsonable_encoder(
                {'id': data.id, 'username': data.username, 'role': data.role, 'datetime': datetime.utcnow()})  # type: ignore
            encoded_jwt = jwt.encode(
                create_access_token, SECRET_KEY, algorithm=ALGORITHM)
            return {"status": "ok", "token": encoded_jwt}
        else :
            return {"status": "failed"}
    except:
        return {"status": "error"}

##-------------------- Authentication user ----------------------##
@app.post("/user/Authentication")
async def user_Authentication(auth: Authentication):
    if auth.token:
        try:
            payload = jwt.decode(auth.token, SECRET_KEY, algorithms=[ALGORITHM])
            return {"status": "ok", "payload": payload}
        except jwt.exceptions.DecodeError:
            return {"status": "failed", "message": "Invalid token"}
    else:
        return {"status": "failed", "message": "Missing token"}

##-------------------- register user ----------------------##
@app.post("/user/register")
async def user_register(user: Customer):
    db.execute(customers.insert().values(
        name=user.name,
        email=user.email,
        phone=user.phone,
        username=user.username,
        password=user.password,
        role='user',
        image=user.image,
    ))
    return {"Successfull!!"}

##-------------------- user get packages, painters ----------------------##
@app.get("/api/painter/{id_painter}")
async def api_painter(id_painter: int):
    data = db.execute(painters.select().where(painters.c.painter_id == id_painter)).fetchall()
    package = db.execute(packages.select().where(packages.c.painter_id == id_painter)).fetchall()
    image = db.execute(images.select().where(images.c.painter_id == id_painter)).fetchall()
    return {'data': data, 'packages': package, 'images': image}

##-------------------- user choose package painters ----------------------##
@app.get("/user/choose/package/{id_package}")
async def api_packages(id_package: int):
    package = db.execute(packages.select().where(packages.c.package_id == id_package)).fetchone()
    return {'package': package}

##-------------------- get mydata User ----------------------##
@app.get("/MydataUser/{token}")
async def mydataUser(token: str):
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    mydata = db.execute(customers.select().where(customers.c.id == payload["id"])).fetchone()
    my_order = db.execute(orders.select().where(orders.c.user_id == payload["id"]).order_by(orders.c.order_create.asc())).fetchall() #ดึงข้อมูล orders ทั้งหมดที่มี status เป็น 0 (ก็คือรอการอนุมัติจากนักวาด)
    my_rating = db.execute(ratings.select().where(ratings.c.user_id == payload["id"])).fetchall() #ดึงข้อมูล orders ทั้งหมดที่มี status เป็น 0 (ก็คือรอการอนุมัติจากนักวาด)
    status = db.execute(order_status.select()).fetchall()
    return {'data': mydata, 'orders': my_order, 'rating': my_rating, 'status': status}  # type: ignore

##-------------------- profile user ----------------------##
@app.get("/user/profile/{id_user}")
async def get_mydata(id_user: int):
    data = db.execute(customers.select().where(customers.c.id == id_user)).fetchall()
    return data

##-------------------- user edit profile ----------------------##
@app.put("/user/edit/profile/{id_user}")
async def user_edit(user_edit: User_edit_profile, id_user: int):
    db.execute(customers.update().values(name=user_edit.name, username=user_edit.username, email=user_edit.email, phone=user_edit.phone).where(customers.c.id == id_user))
    print("--------- Update Success ----------")
    return {"--------- Update Success ----------"}

##-------------------- user get packages, painters ----------------------##
@app.post("/user/order")
async def order_user(order: Order_user):
    db.execute(orders.insert().values(
        user_id=order.user_id,
        painter_id=order.painter_id,
        package_id=order.package_id,
        order_detail=order.order_detail,
        order_image=order.order_image,
        order_status=1,
        user_name=order.user_name,
        user_phone=order.user_phone,
        user_email=order.user_email,
        painter_name=order.painter_name,
        painter_phone=order.painter_phone,
        painter_style=order.painter_style,
        painter_email=order.painter_email,
        package_name=order.package_name,
        package_character=order.package_character,
        package_bgcolor=order.package_bgcolor,
        package_size=order.package_size,
        package_resolution=order.package_resolution,
        package_file=order.package_file,
        package_price=order.package_price,
    ))
    return {"-----------------Successfull!!-----------------"}

##-------------------- user see order detail ----------------------##
@app.get("/user/see/order/{id_order}")
async def user_see_order(id_order: int): 
    user_order = db.execute(orders.select().where(orders.c.order_id == id_order)).fetchone()
    return {'order': user_order}

##-------------------- user view product ----------------------##
@app.get("/user/view/product/{id_order}")
async def user_view_product(id_order: int):
    product = db.execute(products.select().where(products.c.order_id == id_order)).fetchone()
    return {'product': product}

##-------------------- user cancel order ----------------------##
@app.put("/user/cancel/order/{id_order}")
async def user_cancel_order(id_order: int, cancel: Order_disapprove):
    db.execute(orders.update().values(order_status=cancel.order_status).where(orders.c.order_id == id_order)) ## เมื่อยกเลิกจะเปลี่ยนสถานะเป็น 2 คือยกเลิกการจอง
    print("--------- Cancel Success!! ----------")
    return {"--------- Cancel Success!! ----------"}
    
##-------------------- Update status ----------------------##
@app.put("/user/status/order/{id_order}")
async def user_confirm_order(id_order: int, update_status: Order_confirm):
    db.execute(orders.update().values(order_status=update_status.order_status).where(orders.c.order_id == id_order)) ## เมื่อยกเลิกจะเปลี่ยนสถานะเป็น 2 คือยกเลิกการจอง
    print("--------- Confirm Success!! ----------")
    return {"--------- Confirm Success!! ----------"}

##-------------------- register user ----------------------##
@app.post("/user/click/rating")
async def user_rating(rating: User_give_rating):
    db.execute(ratings.insert().values(
        user_id=rating.user_id,
        painter_id=rating.painter_id,
        rating=rating.rating
    ))
    print("--------- Successfull!! ----------")
    return {"--------- Successfull!! ----------"}
    

###--------------------------------------- Painter -----------------------------------------###

##-------------------- Data API painters ----------------------##
@app.get("/api/painters")
async def api_painters():
    data = db.execute(painters.select()).fetchall()
    image_homepage = db.execute(images.select()).fetchall()
    return {'data': data, 'images': image_homepage}

##-------------------- data, packages painter ----------------------##
@app.get("/MydataPainter/{token}")
async def mydataPainter(token: str):
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    data = db.execute(painters.select().where(painters.c.painter_id == payload["id"])).fetchall()
    my_package = db.execute(packages.select().where(packages.c.painter_id == payload["id"])).fetchall()
    my_image = db.execute(images.select().where(images.c.painter_id == payload["id"])).fetchall()
    my_order = db.execute(orders.select().where(orders.c.painter_id == payload["id"], orders.c.order_status <= 4).order_by(orders.c.order_create.asc())).fetchall() #ดึงข้อมูล orders ทั้งหมดที่มี status เป็น 0 (ก็คือรอการอนุมัติจากนักวาด)
    status = db.execute(order_status.select()).fetchall()
    return {'data': data, 'packages': my_package, 'images': my_image, 'orders': my_order, 'status': status}  # type: ignore

##-------------------- login painter ----------------------##
@app.post("/painter/login")
async def painter_login(login: LoginPainter):
    try:
        data = db.execute(painters.select().where(painters.c.painter_username == login.username and painters.c.painter_password == login.password)).first()
        if data.painter_username == login.username and data.painter_password == login.password: # type: ignore
            create_access_token = jsonable_encoder(
                {'id': data.painter_id, 'username': data.painter_username, 'role': data.role, 'datetime': datetime.utcnow()})  # type: ignore
            encoded_jwt = jwt.encode(
                create_access_token, SECRET_KEY, algorithm=ALGORITHM)
            return {"status": "ok", "token": encoded_jwt}
        else :
            return {"status": "failed"}
    except:
        return {"status": "error"}

##-------------------- Authentication painter ----------------------##
@app.post("/painter/Authentication")
async def painter_Authentication(auth: Painter_Authentication):
    if auth.token:
        try:
            payload = jwt.decode(auth.token, SECRET_KEY, algorithms=[ALGORITHM])
            return {"status": "ok", "payload": payload}
        except jwt.exceptions.DecodeError:
            return {"status": "failed", "message": "Invalid token"}
    else:
        return {"status": "failed", "message": "Missing token"}

##-------------------- register painter ----------------------##
@app.post("/painter/register")
async def painter_register(painter: Painter):
    db.execute(painters.insert().values(
        painter_name=painter.painter_name,
        painter_email=painter.painter_email,
        painter_phone=painter.painter_phone,
        painter_style=painter.painter_style,
        painter_username=painter.painter_username,
        painter_password=painter.painter_password,
        role='painter',
        painter_image=painter.painter_image,
    ))
    return {"Successfull!!"}

##-------------------- Navigate profile painter ----------------------##
@app.get("/painter/profile/{id_painter}")
async def profile_painter(id_painter: str):
    data = db.execute(painters.select().where(painters.c.painter_id == id_painter)).fetchall()
    # print("Data User:", data)
    return data

##-------------------- painter edit profile ----------------------##
@app.put("/painter/edit/profile/{id_painter}")
async def painter_edit_profile(painter_edit: Painter_edit_profile, id_painter: str):
    db.execute(painters.update().values(painter_name=painter_edit.name, painter_username=painter_edit.username, painter_email=painter_edit.email, painter_phone=painter_edit.phone).where(painters.c.painter_id == id_painter))  # type: ignore
    # res = db.execute(painters.select().where(painters.c.painter_id == id_painter)).first()
    print("--------- Update Success ----------")
    return {"--------- Update Success ----------"}

##-------------------- get package,resulution image painter ----------------------##
@app.get("/painter/package/{id_package}")
async def package_painter(id_package: str):
    data = db.execute(packages.select().where(packages.c.package_id == id_package)).fetchall()
    # print("Data User:", data)
    return data

##-------------------- painter add package ----------------------##
@app.post("/painter/add/package")
async def painter_add_package(painter_add: Painter_add_package):
    db.execute(packages.insert().values(
        painter_id=painter_add.painter_id,
        package_name=painter_add.package_name,
        package_character=painter_add.package_character,
        package_bgcolor=painter_add.package_bgcolor,
        package_size=painter_add.package_size,
        package_resolution=painter_add.package_resolution,
        package_file=painter_add.package_file,
        package_price=painter_add.package_price,
    ))
    return {"Successfull!!"}

##-------------------- painter edit package ----------------------##
@app.put("/painter/edit/package/{id_package}")
async def painter_edit_package(painter_edit: Painter_edit_package, id_package: str):
    db.execute(packages.update().values(package_name=painter_edit.package_name, package_character=painter_edit.package_character, package_bgcolor=painter_edit.package_bgcolor, package_size=painter_edit.package_size, package_resolution=painter_edit.package_resolution, package_file=painter_edit.package_file, package_price=painter_edit.package_price,).where(packages.c.package_id == id_package))
    res = db.execute(packages.select().where(packages.c.package_id == id_package)).first()
    print("--------- Update Success ----------")
    return res

##-------------------- painter delete package ----------------------##
@app.delete("/painter/delete/package/{id_package}")
async def painter_delete_package(id_package: str):
    db.execute(packages.delete().where(packages.c.package_id == id_package))
    # res = db.execute(customers.select().where(customers.c.id == id_user)).first()
    print("--------- Delete Success ----------")
    return {"--------- Delete Success ----------"}

##-------------------- painter add image ----------------------##
@app.post("/painter/add/image")
async def painter_add_image(painter_add: Painter_Add_Image):
    db.execute(images.insert().values(
        painter_id=painter_add.painter_id,
        image_base64=painter_add.image_base64,
    ))
    return {"--------- Successfull!! ----------"}

##-------------------- painter delete image ----------------------##
@app.delete("/painter/delete/image/{image_id}")
async def painter_delete_image(image_id: str):
    db.execute(images.delete().where(images.c.image_id == image_id))
    # res = db.execute(customers.select().where(customers.c.id == id_user)).first()
    print("--------- Delete Success ----------")
    return {"--------- Delete Success ----------"}

##-------------------- Painter click approve  ----------------------##
@app.put("/painter/click/approve/{id_order}")
async def painter_click_approve(id_order: str, approve: Order_approve):
    db.execute(orders.update().values(
        order_status=approve.order_status,
        order_dateQ=approve.order_dateQ,
        order_deadline=approve.order_deadline).where(orders.c.order_id == id_order)) ## เมื่อยกเลิกจะเปลี่ยนสถานะเป็น 2 คือยกเลิกการจอง
    print("--------- Approve Success!! ----------")
    return {"--------- Approve Success!! ----------"}

##-------------------- Painter click disapprove ----------------------##
@app.put("/painter/click/disapprove/{id_order}")
async def painter_click_disapprove(id_order: str, cancel: Order_disapprove):
    db.execute(orders.update().values(order_status=cancel.order_status).where(orders.c.order_id == id_order)) ## เมื่อยกเลิกจะเปลี่ยนสถานะเป็น 2 คือยกเลิกการจอง
    print("--------- Cancel Success!! ----------")
    return {"--------- Cancel Success!! ----------"}

##-------------------- Painter click add queue ----------------------##
@app.put("/painter/click/addqueue/{id_order}")
async def painter_click_addqueue(id_order: str, Add_queue: Painter_add_queue):
    db.execute(orders.update().values(order_status=Add_queue.order_status).where(orders.c.order_id == id_order)) ## เมื่อยกเลิกจะเปลี่ยนสถานะเป็น 2 คือยกเลิกการจอง
    print("--------- Add_queue Success!! ----------")
    return {"--------- Add_queue Success!! ----------"}
    

##-------------------- Painter see order detail ----------------------##
@app.get("/painter/see/order_deteil/{id_order}")
async def painter_see_order_deteil(id_order: str):
    order = db.execute(orders.select().where(orders.c.order_id == id_order)).fetchone()
    return {'order': order}

##-------------------- Painter see ALL queue ----------------------##
@app.get("/painter/see/All_queue/{token}")
async def painter_see_queue(token: str):
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    all_order = db.execute(orders.select().where(orders.c.painter_id == payload["id"], orders.c.order_status >= 5).order_by(orders.c.order_dateQ.asc())).fetchall()
    status = db.execute(order_status.select().where(order_status.c.status_id >= 5)).fetchall()
    return {'orders': all_order, 'status': status}  # type: ignore

##-------------------- Painter click Status ----------------------##
@app.put("/painter/click/status/{id_order}")
async def painter_click_status(id_order: str, status: Painter_change_status):
    db.execute(orders.update().values(order_status=status.order_status).where(orders.c.order_id == id_order)) ## เมื่อยกเลิกจะเปลี่ยนสถานะเป็น 2 คือยกเลิกการจอง
    print("--------- Change Status Success!! ----------")
    return {"--------- Change Status Success!! ----------"}

##-------------------- painter submit work ----------------------##
@app.put("/painter/submitwork/image/{id_order}")
async def painter_submitwork(product: Painter_submit_work, id_order: str):
    db.execute(orders.update().values(
        order_id=id_order,
        order_status=product.order_status,
    ).where(orders.c.order_id == id_order))

    db.execute(products.insert().values(
        order_id=id_order,
        product_img=product.product_img,
    ))
    return {"--------- Successfull!! ----------"}



###--------------------------------------- Admin -----------------------------------------###

##-------------------- login admin ----------------------##
@app.post("/admin/login")
async def admin_login(dataAdmin: LoginAdmin):
    try:
        data = db.execute(admin.select().where(admin.c.admin_username == dataAdmin.username and admin.c.admin_password == dataAdmin.password)).first()
        if data.admin_username == dataAdmin.username and data.admin_password == dataAdmin.password: # type: ignore
            create_access_token = jsonable_encoder(
                {'id': data.admin_id, 'username': data.admin_username, 'role': data.role, 'datetime': datetime.utcnow()})  # type: ignore
            encoded_jwt = jwt.encode(
                create_access_token, SECRET_KEY, algorithm=ALGORITHM)
            return {"status": "ok", "token": encoded_jwt}
        else :
            return {"status": "failed"}
    except:
        return {"status": "error"}

##-------------------- Authentication admin ----------------------##
@app.post("/admin/Authentication")
async def admin_Authentication(auth: Admin_Authentication):
    if auth.token:
        try:
            payload = jwt.decode(auth.token, SECRET_KEY, algorithms=[ALGORITHM])
            return {"status": "ok", "payload": payload}
        except jwt.exceptions.DecodeError:
            return {"status": "failed", "message": "Invalid token"}
    else:
        return {"status": "failed", "message": "Missing token"}

##-------------------- get user ----------------------##
@app.get("/admin/get/user/{id_user}")
async def admin_customers(id_user: str):
    data = db.execute(customers.select().where(customers.c.id == id_user)).fetchall()
    return data

##-------------------- admin update user ----------------------##
@app.put("/admin/update/user/{id_user}")
async def admin_update(data_update: Admin_update, id_user: str):
    db.execute(customers.update().values(
        name=data_update.name, 
        username=data_update.username, 
        email=data_update.email,
        phone=data_update.phone).where(customers.c.id == id_user))
    res = db.execute(customers.select().where(customers.c.id == id_user)).first()
    print("--------- Update Success ----------")
    return res

##-------------------- admin delete user ----------------------##
@app.delete("/admin/delete/user/{id_user}")
async def admin_delete(id_user: str):
    db.execute(customers.delete().where(customers.c.id == id_user))
    print("--------- Delete Success ----------")
    return {"--------- Delete Success ----------"}




##-------------------- get painter ----------------------##
@app.get("/admin/get/painter/{id_painter}")
async def admin_painter(id_painter: str):
    data = db.execute(painters.select().where(painters.c.painter_id == id_painter)).fetchall()
    return data

##-------------------- admin update painter ----------------------##
@app.put("/admin/update/painter/{id_painter}")
async def admin_update_painter(data_update: Admin_update, id_painter: str):
    db.execute(painters.update().values(painter_name=data_update.name, painter_username=data_update.username, painter_email=data_update.email, painter_phone=data_update.phone).where(painters.c.painter_id == id_painter))
    res = db.execute(painters.select().where(painters.c.painter_id == id_painter)).first()
    print("--------- Update Success ----------")
    return res

##-------------------- admin delete painter ----------------------##
@app.delete("/admin/delete/painter/{id_painter}")
async def admin_delete_painter(id_painter: str):
    db.execute(painters.delete().where(painters.c.painter_id == id_painter))
    print("--------- Delete Success ----------")
    return {"--------- Delete Success ----------"}

if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=80, debug=True)  # type: ignore
