from pydantic import BaseModel

#----------------------- Schemas user -------------------#
class Customer(BaseModel):
    name: str
    email: str
    phone: str
    username: str
    password: str
    image: str

class LoginItem(BaseModel):
    username: str
    password: str

class Authentication(BaseModel):
    token: str

class User_edit_profile(BaseModel):
    name: str
    username: str
    email: str
    phone: str

class Order_user(BaseModel):
    user_id: int
    painter_id: int
    package_id: int
    order_detail: str
    order_image: str
    user_name: str
    user_phone: str
    user_email: str
    painter_name: str
    painter_phone: str
    painter_style: str
    painter_email: str
    package_name: str
    package_character: str
    package_bgcolor: str
    package_size: str
    package_resolution: str
    package_file: str
    package_price: str

class Order_approve(BaseModel):
    order_status: int
    order_dateQ: str
    order_deadline: str

class Order_disapprove(BaseModel):
    order_status: int

class Order_confirm(BaseModel):
    order_status: int

class User_give_rating(BaseModel):
    user_id: int
    painter_id: int
    rating: str

#----------------------- Schemas painter -------------------#
class Painter(BaseModel):
    painter_name: str
    painter_email: str
    painter_phone: str
    painter_style: str
    painter_username: str
    painter_password: str
    painter_image: str

class LoginPainter(BaseModel):
    username: str
    password: str

class Painter_Authentication(BaseModel):
    token: str

class Painter_edit_profile(BaseModel):
    name: str
    username: str
    email: str
    phone: str

class Painter_add_package(BaseModel):
    painter_id: int
    package_name: str
    package_character: str
    package_bgcolor: str
    package_size: str
    package_resolution: str
    package_file: str
    package_price: str

class Painter_edit_package(BaseModel):
    package_name: str
    package_character: str
    package_bgcolor: str
    package_size: str
    package_resolution: str
    package_file: str
    package_price: str

class Painter_Image(BaseModel):
    painter_id: int
    image_id: int
    image_base64: str

class Painter_Add_Image(BaseModel):
    painter_id: int
    image_base64: str

class Painter_add_queue(BaseModel):
    order_status: int

class Painter_change_status(BaseModel):
    order_status: int

class Painter_submit_work(BaseModel):
    order_status: int
    product_img: str

#----------------------- Schemas admin -------------------#
class LoginAdmin(BaseModel):
    username: str
    password: str

class Admin_Authentication(BaseModel):
    token: str

class Admin(BaseModel):
    admin_name: str
    admin_email: str
    admin_username: str
    admin_password: str

class Admin_update(BaseModel):
    name: str
    email: str
    username: str
    phone: str

